//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ServerLogicDataBase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TServerMethods1::TServerMethods1(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::ReverseString(System::UnicodeString value)
{
	return ::ReverseString(value);
}
//----------------------------------------------------------------------------          // Insert Order

void TServerMethods1::ConfirmOrder(	double orderDate, double orderTime, int productID,
									double Price, UnicodeString FIO, UnicodeString Phone,
									UnicodeString Address, UnicodeString Notes)
{
	Ordering_insProc->Params->ParamByName("CREATE_DATETIME")->Value 	= Now();
	Ordering_insProc->Params->ParamByName("ORDER_DATE")		->Value		= orderDate;
	Ordering_insProc->Params->ParamByName("ORDER_TIME")		->Value 	= orderTime;
	Ordering_insProc->Params->ParamByName("PRODUCT_ID")		->Value 	= productID;
	Ordering_insProc->Params->ParamByName("PRICE")			->Value 	= Price;
	Ordering_insProc->Params->ParamByName("FIO")			->Value 	= FIO;
	Ordering_insProc->Params->ParamByName("PHONE")			->Value 	= Phone;
	Ordering_insProc->Params->ParamByName("ADDRESS")		->Value 	= Address;
	Ordering_insProc->Params->ParamByName("NOTES")			->Value 	= Notes;
	Ordering_insProc->Params->ParamByName("STATUS")			->Value 	= 0;
	Ordering_insProc->ExecProc();
}
//----------------------------------------------------------------------------          // Insert Feedback

void TServerMethods1::ConfirmFeedback(	UnicodeString FIO, UnicodeString Phone,
										UnicodeString E_mail, UnicodeString Notes,
										int Rating)
{
	//Feedback_insProc->Params->ParamByName("CREATE_DATETIME")->Value 	= Now();
	Feedback_insProc->Params->ParamByName("FIO")			->Value 	= FIO;
	Feedback_insProc->Params->ParamByName("PHONE")			->Value 	= Phone;
	Feedback_insProc->Params->ParamByName("E_MAIL")			->Value 	= E_mail;
	Feedback_insProc->Params->ParamByName("NOTES")			->Value 	= Notes;
	Feedback_insProc->Params->ParamByName("RATING")			->Value 	= Rating;
	Feedback_insProc->ExecProc();
}


