//----------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
//----------------------------------------------------------------------------
USEFORM("ServerLogicDataBase.cpp", ServerMethods1); /* TDSServerModule: File Type */
USEFORM("ServerBase.cpp", ServerContainer1); /* TDataModule: File Type */
//---------------------------------------------------------------------------
extern void runDSServer();
//----------------------------------------------------------------------------
#pragma argsused
int _tmain(int argc, _TCHAR* argv[])
{
  try
  {
    runDSServer();
  }
  catch (Exception &exception)
  {
    printf("%ls: %ls", exception.ClassName().c_str(), exception.Message.c_str());
  }
  return 0;
}
//----------------------------------------------------------------------------

