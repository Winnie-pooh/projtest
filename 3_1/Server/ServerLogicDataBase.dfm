object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 341
  Width = 500
  object PizzadbConnection: TSQLConnection
    ConnectionName = 'PizzaDB'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=D:/PIZZADB.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    Connected = True
    Left = 195
    Top = 15
  end
  object taOrder: TSQLDataSet
    CommandText = 'ORDERING'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzadbConnection
    Left = 83
    Top = 111
  end
  object taProduct: TSQLDataSet
    CommandText = 'PRODUCT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzadbConnection
    Left = 193
    Top = 113
  end
  object taRestaurant: TSQLDataSet
    CommandText = 'RESTAURANT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzadbConnection
    Left = 310
    Top = 111
  end
  object dspOrder: TDataSetProvider
    DataSet = taOrder
    Left = 72
    Top = 200
  end
  object dspProduct: TDataSetProvider
    DataSet = taProduct
    Left = 192
    Top = 200
  end
  object dspRestaurant: TDataSetProvider
    DataSet = taRestaurant
    Left = 312
    Top = 200
  end
  object Ordering_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftTimeStamp
        Precision = 8
        Name = 'CREATE_DATETIME'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Precision = 4
        Name = 'ORDER_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftTime
        Precision = 4
        Name = 'ORDER_TIME'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRODUCT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftSingle
        Precision = 4
        Name = 'PRICE'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 70
        Name = 'FIO'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 20
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 255
        Name = 'ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 4000
        Name = 'NOTES'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'STATUS'
        ParamType = ptInput
      end>
    SQLConnection = PizzadbConnection
    StoredProcName = 'ORDERING_INS'
    Left = 190
    Top = 276
  end
  object taFeedback: TSQLDataSet
    CommandText = 'FEEDBACK'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = PizzadbConnection
    Left = 411
    Top = 115
  end
  object dspFeedback: TDataSetProvider
    DataSet = taFeedback
    Left = 408
    Top = 200
  end
  object Feedback_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftWideString
        Precision = 70
        Name = 'FIO'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 20
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 40
        Name = 'E_MAIL'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 4000
        Name = 'NOTES'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'RATING'
        ParamType = ptInput
      end>
    SQLConnection = PizzadbConnection
    StoredProcName = 'FEEDBACK_INS'
    Left = 293
    Top = 275
  end
end
