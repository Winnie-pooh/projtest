//---------------------------------------------------------------------------

#ifndef ServerLogicDataBaseH
#define ServerLogicDataBaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TServerMethods1 : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *PizzadbConnection;
	TSQLDataSet *taOrder;
	TSQLDataSet *taProduct;
	TSQLDataSet *taRestaurant;
	TDataSetProvider *dspOrder;
	TDataSetProvider *dspProduct;
	TDataSetProvider *dspRestaurant;
	TSQLStoredProc *Ordering_insProc;
	TSQLDataSet *taFeedback;
	TDataSetProvider *dspFeedback;
	TSQLStoredProc *Feedback_insProc;


private:	// User declarations
public:		// User declarations
	__fastcall TServerMethods1(TComponent* Owner); 
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);

	//  My functions

    void TServerMethods1::ConfirmOrder(	double orderDate, double orderTime, int productID,
										double Price, UnicodeString aFIO, UnicodeString Phone,
										UnicodeString Address, UnicodeString Notes);

	void TServerMethods1::ConfirmFeedback(	UnicodeString FIO, UnicodeString Phone,
											UnicodeString E_mail, UnicodeString Notes,
											int Rating);

};
//---------------------------------------------------------------------------
extern PACKAGE TServerMethods1 *ServerMethods1;
//---------------------------------------------------------------------------
#endif

