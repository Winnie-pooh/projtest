//---------------------------------------------------------------------------

#ifndef unitFrameRestaurantH
#define unitFrameRestaurantH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class TframeRestaurant : public TFrame
{
__published:	// IDE-managed Components
	TLabel *labelFrameRestaurantName;
	TText *textFrameRestaurantAddress;
	TImage *imageFrameRestaurant;
private:	// User declarations
public:		// User declarations
	__fastcall TframeRestaurant(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TframeRestaurant *frameRestaurant;
//---------------------------------------------------------------------------
#endif
