//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "ClientFormMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------   // Form Create
void __fastcall TformMain::FormCreate(TObject *Sender)
{
	myRating           = 4;

	tcMain->ActiveTab 	= tiMainPage;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonToTermsPageClick(TObject *Sender)
{
	tcMain->GotoVisibleTab(tiTermsPage->Index);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonGoHomeClick(TObject *Sender)
{
	tcMain->GotoVisibleTab(tiMainPage->Index);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonToContactsPageClick(TObject *Sender)
{
	tcMain->GotoVisibleTab(tiContactsPage->Index);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonBackToAllFeedbackClick(TObject *Sender)
{
	tcMain->GotoVisibleTab(tiAllFeedbackPage->Index);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonToFeedbackCreatePageClick(TObject *Sender)
{
	tcMain->GotoVisibleTab(tiMakeFeedbackPage->Index);
}
//---------------------------------------------------------------------------   // GoTo AllPizzaPage
void __fastcall TformMain::buttonToPizzaPageClick(TObject *Sender)
{
	serverConLogicForm->cdsProduct->First();
	try {
		while(!serverConLogicForm->cdsProduct->Eof)
		{
			if(glList->FindComponent("frProductCell"+IntToStr(serverConLogicForm->cdsProductID->Value)))
			{
				break;
			}

			TframePizza *x = new TframePizza(glList);

			x->Parent 	= glList;
			x->Align 	= TAlignLayout::Client;
			x->Name 	= "frProductCell"+IntToStr(serverConLogicForm->cdsProductID->Value);
			x->Tag 		= serverConLogicForm->cdsProductID->Value;

			x->labelFramePizzaName->Text 	= serverConLogicForm->cdsProductNAME->Value;
			x->labelFramePrice->Text 		= FloatToStr(serverConLogicForm->cdsProductPRICE->Value);
			x->imageFrame->Bitmap->Assign(serverConLogicForm->cdsProductIMAGE);

			x->imageFrame->OnClick = ToPizzaItemPageClick;

			if(serverConLogicForm->cdsProductSTICKER->Value==1)
                x->imageFrameSticker->Visible = true;

			serverConLogicForm->cdsProduct->Next();

		   //break;
		}
		glList->RecalcSize();
	}catch (...) {
	}

	//
	tcMain->GotoVisibleTab(tiAllPizzaPage->Index);

}
//---------------------------------------------------------------------------   // Resize glList(Pizza&Restau)
void __fastcall TformMain::glListResize(TObject *Sender)
{
	TGridLayout *x = (TGridLayout *)Sender;
	// ����������� ������ ������
	float xNewWidth = (x->Width < 400) ? x->Width: x->Width/2;
	//glList->ItemHeight = (glList->ItemHeight*xNewWidth)/glList->ItemWidth;
	float  coefficient;
	if(x->Name == "glList")
		coefficient = (float)514/350;
	else
		coefficient = (float)400/300;
	x->ItemHeight 		= (float)x->ItemWidth/coefficient;
	x->ItemWidth = xNewWidth;
	// ������������ ������ ����������, ����� ������ ��� ������ �����
	int k = Ceil((x->ComponentCount)/(x->Width/x->ItemWidth));
	x->Height = k*x->ItemHeight;
}
//---------------------------------------------------------------------------   // GoTo PizzaItemPage
void __fastcall TformMain::ToPizzaItemPageClick(TObject *Sender)
{
	int aID     = ((TControl*)Sender)->Parent->Tag;
	TLocateOptions xLO;

	serverConLogicForm->cdsProduct->Locate(serverConLogicForm->cdsProductID->FieldName, aID, xLO);
	//
	labelPizzaItemCaption->Text = serverConLogicForm->cdsProductNAME->Value;
	imagePizzaItem->Bitmap->Assign(serverConLogicForm->cdsProductIMAGE);
//	laProductWeight->Text = FormatFloat("0.000", dm->cdsProductWEIGHT->Value);
	labelPizzaItemPrice->Text = FloatToStr(serverConLogicForm->cdsProductPRICE->Value) + L" ���";
	textPizzaItemDescription->Text = serverConLogicForm->cdsProductNOTES->Value;


	tcMain->GotoVisibleTab(tiPizzaItemPage->Index);
}
//---------------------------------------------------------------------------   // GoTo OrderPage
void __fastcall TformMain::buttonToOrderCreatePageClick(TObject *Sender)
{
	labelPizzaOrderName->Text = serverConLogicForm->cdsProductNAME->Value;
	labelPizzaOrderPrice->Text = L"����: " + FloatToStr(
											serverConLogicForm->cdsProductPRICE->Value)
											+ L" ���";

	dataEditOrderDate->DateTime	= Now()+1;
	timeEditOrderTime->Time 	= Now();

	tcMain->GotoVisibleTab(tiMakeOrderPage->Index);
}
//---------------------------------------------------------------------------   // Maker Order
void __fastcall TformMain::buttonOrderConfirmationClick(TObject *Sender)
{
	serverConLogicForm->ServerMethods1Client->ConfirmOrder(

			dataEditOrderDate->Date,
			timeEditOrderTime->Time,
			serverConLogicForm->cdsProductID->Value,
			serverConLogicForm->cdsProductPRICE->Value,
			editOrderFIO->Text,
			editOrderPhone->Text,
			memoOrderAddress->Text,
			memoOrderNotes->Text
	);

	serverConLogicForm->cdsOrder->Refresh() ;
	ShowMessage(L"����� ���������");
	tcMain->GotoVisibleTab(tiAllPizzaPage->Index);
}
//--------------------------------------------------------------------------- 	// GoTo AllRestaurantPage
void __fastcall TformMain::buttonToRestaurantPageClick(TObject *Sender)
{
	serverConLogicForm->cdsRestaurant->First();
	try {
		while(!serverConLogicForm->cdsRestaurant->Eof)
		{
			if(glListRestaurant->FindComponent("frRestaurantCell"+IntToStr(serverConLogicForm->cdsRestaurantID->Value)))
			{
				break;
			}

			TframeRestaurant *x = new TframeRestaurant(glListRestaurant);

			x->Parent 	= glListRestaurant;
			x->Align 	= TAlignLayout::Client;
			x->Name 	= "frRestaurantCell"+IntToStr(serverConLogicForm->cdsRestaurantID->Value);
			x->Tag 		= serverConLogicForm->cdsRestaurantID->Value;

			x->labelFrameRestaurantName->Text 	= serverConLogicForm->cdsRestaurantNAME->Value;
			x->textFrameRestaurantAddress->Text = serverConLogicForm->cdsRestaurantADDRESS->Value;

			TMemoryStream *s = new TMemoryStream();
			serverConLogicForm->cdsRestaurantMAP->SaveToStream(s);
			x->imageFrameRestaurant->Bitmap->LoadFromStream(s);

			x->imageFrameRestaurant->OnClick = ToRestaurantItemPageClick;

			serverConLogicForm->cdsRestaurant->Next();
            s->DisposeOf();
		}
		glListRestaurant->RecalcSize();
	}catch (...) {
	}

	tcMain->GotoVisibleTab(tiAllRestaurantPage->Index);
}
//---------------------------------------------------------------------------   // GoTo RestaurantItemPage
void __fastcall TformMain::ToRestaurantItemPageClick(TObject *Sender)
{
	serverConLogicForm->cdsRestaurant->First();
	TMemoryStream *s = new TMemoryStream();
	int aID     = ((TControl*)Sender)->Parent->Tag;
	TLocateOptions xLO;

	serverConLogicForm->cdsRestaurant->Locate(serverConLogicForm->cdsRestaurantID->FieldName, aID, xLO);

	//
	labelRestaurantCaption->Text	= serverConLogicForm->cdsRestaurantNAME->Value;
    serverConLogicForm->cdsRestaurantMAP->SaveToStream(s);
	imageRestaurantItem->Bitmap->LoadFromStream(s);

	textRestaurantAddress->Text = serverConLogicForm->cdsRestaurantADDRESS->Value;
	textRestaurantHours->Text   = serverConLogicForm->cdsRestaurantHOURS->Value;

	tcMain->GotoVisibleTab(tiRestaurantItemPage->Index);
}
//---------------------------------------------------------------------------   // GoTo AllFeedbackPage
void __fastcall TformMain::buttonToFeedbackPageClick(TObject *Sender)
{
	serverConLogicForm->cdsFeedback->First();
	try {
		while(!serverConLogicForm->cdsFeedback->Eof)
		{
			if(glListFeedback->FindComponent("frFeedbackCell"+IntToStr(serverConLogicForm->cdsFeedbackID->Value)))
			{
				break;
			}

			TframeFeedback *x = new TframeFeedback(glListFeedback);

			x->Parent 	= glListFeedback;
			x->Align 	= TAlignLayout::Client;
			x->Name 	= "frFeedbackCell"+IntToStr(serverConLogicForm->cdsFeedbackID->Value);
			x->Tag 		= serverConLogicForm->cdsFeedbackID->Value;

			x->labelFeedbackItemFIO->Text 		= serverConLogicForm->cdsFeedbackFIO->Value;
			x->labelFeedbackItemRating->Text 	= L"������: " + IntToStr(serverConLogicForm->cdsFeedbackRATING->Value);
			x->memoFeedbackItemNote->Text       = serverConLogicForm->cdsFeedbackNOTES->Value;

			serverConLogicForm->cdsFeedback->Next();
		}
		glListFeedback->RecalcSize();
	}catch (...) {
	}


	tcMain->GotoVisibleTab(tiAllFeedbackPage->Index);
}
//---------------------------------------------------------------------------   // Change Rating
void __fastcall TformMain::RadioButtonChange(TObject *Sender)
{
	myRating = ((TComponent*)Sender)->Tag;
}
//---------------------------------------------------------------------------   // Resize glListFeedback
void __fastcall TformMain::glListFeedbackResize(TObject *Sender)
{
	TGridLayout *x = (TGridLayout *)Sender;
	// ����������� ������ ������
	float xNewWidth = (x->Width < 300) ? x->Width: x->Width/(Ceil(x->Width/300));

	float  coefficient = 1.5;
	x->ItemHeight 		= (float)x->ItemWidth/coefficient;
	x->ItemWidth = xNewWidth;
	// ������������ ������ ����������, ����� ������ ��� ������ �����
	int k = Ceil((x->ComponentCount)/(x->Width/x->ItemWidth));
	x->Height = k*x->ItemHeight;
}
//---------------------------------------------------------------------------   // Maker Feedback
void __fastcall TformMain::buttonFeedbackConfirmationClick(TObject *Sender)
{
	serverConLogicForm->ServerMethods1Client->ConfirmFeedback(

			editFeedbackFIO->Text,
			editFeedbackPhone->Text,
			editFeedbackMail->Text,
			memoFedbackNote->Text,
			myRating
	);

	serverConLogicForm->cdsFeedback->Refresh();
	//
	//                  TODO: Bring to one function
	//
	serverConLogicForm->cdsFeedback->Next();
	TframeFeedback *x = new TframeFeedback(glListFeedback);

	x->Parent 	= glListFeedback;
	x->Align 	= TAlignLayout::Client;
	x->Name 	= "frFeedbackCell"+IntToStr(serverConLogicForm->cdsFeedbackID->Value);
	x->Tag 		= serverConLogicForm->cdsFeedbackID->Value;

	x->labelFeedbackItemFIO->Text 		= serverConLogicForm->cdsFeedbackFIO->Value;
	x->labelFeedbackItemRating->Text 	= L"������: " + IntToStr(serverConLogicForm->cdsFeedbackRATING->Value);
	x->memoFeedbackItemNote->Text       = serverConLogicForm->cdsFeedbackNOTES->Value;
	///
	///
	//

	ShowMessage(L"������� �� �����!");
	tcMain->GotoVisibleTab(tiAllFeedbackPage->Index);
	//Clear editFields
	editFeedbackFIO->Text 	= "";
	editFeedbackPhone->Text = "";
	editFeedbackMail->Text  = "";
	memoFedbackNote->Text   = "";
	myRating                = 4;
    RadioButton3->IsChecked = true;

}
//---------------------------------------------------------------------------

