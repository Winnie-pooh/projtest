//---------------------------------------------------------------------------

#ifndef unitFramePizzaElementH
#define unitFramePizzaElementH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TframePizza : public TFrame
{
__published:	// IDE-managed Components
	TPanel *panelFrame;
	TImage *imageFrame;
	TLabel *labelFramePizzaName;
	TImage *imageFrameSticker;
	TLayout *Layout1;
	TLayout *Layout2;
	TLabel *labelFramePrice;
private:	// User declarations
public:		// User declarations
	__fastcall TframePizza(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TframePizza *framePizza;
//---------------------------------------------------------------------------
#endif
