//
// Created by the DataSnap proxy generator.
// 19.01.2018 17:04:02
//

#include "uProxy.h"

System::UnicodeString __fastcall TServerMethods1Client::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TServerMethods1.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TServerMethods1.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TServerMethods1Client::ConfirmOrder(double orderDate, double orderTime, int productID, double Price, System::UnicodeString FIO, System::UnicodeString Phone, System::UnicodeString Address, System::UnicodeString Notes)
{
  if (FConfirmOrderCommand == NULL)
  {
    FConfirmOrderCommand = FDBXConnection->CreateCommand();
    FConfirmOrderCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FConfirmOrderCommand->Text = "TServerMethods1.ConfirmOrder";
    FConfirmOrderCommand->Prepare();
  }
  FConfirmOrderCommand->Parameters->Parameter[0]->Value->SetDouble(orderDate);
  FConfirmOrderCommand->Parameters->Parameter[1]->Value->SetDouble(orderTime);
  FConfirmOrderCommand->Parameters->Parameter[2]->Value->SetInt32(productID);
  FConfirmOrderCommand->Parameters->Parameter[3]->Value->SetDouble(Price);
  FConfirmOrderCommand->Parameters->Parameter[4]->Value->SetWideString(FIO);
  FConfirmOrderCommand->Parameters->Parameter[5]->Value->SetWideString(Phone);
  FConfirmOrderCommand->Parameters->Parameter[6]->Value->SetWideString(Address);
  FConfirmOrderCommand->Parameters->Parameter[7]->Value->SetWideString(Notes);
  FConfirmOrderCommand->ExecuteUpdate();
}

void __fastcall TServerMethods1Client::ConfirmFeedback(System::UnicodeString FIO, System::UnicodeString Phone, System::UnicodeString E_mail, System::UnicodeString Notes, int Rating)
{
  if (FConfirmFeedbackCommand == NULL)
  {
    FConfirmFeedbackCommand = FDBXConnection->CreateCommand();
    FConfirmFeedbackCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FConfirmFeedbackCommand->Text = "TServerMethods1.ConfirmFeedback";
    FConfirmFeedbackCommand->Prepare();
  }
  FConfirmFeedbackCommand->Parameters->Parameter[0]->Value->SetWideString(FIO);
  FConfirmFeedbackCommand->Parameters->Parameter[1]->Value->SetWideString(Phone);
  FConfirmFeedbackCommand->Parameters->Parameter[2]->Value->SetWideString(E_mail);
  FConfirmFeedbackCommand->Parameters->Parameter[3]->Value->SetWideString(Notes);
  FConfirmFeedbackCommand->Parameters->Parameter[4]->Value->SetInt32(Rating);
  FConfirmFeedbackCommand->ExecuteUpdate();
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TServerMethods1Client::~TServerMethods1Client()
{
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FConfirmOrderCommand;
  delete FConfirmFeedbackCommand;
}


