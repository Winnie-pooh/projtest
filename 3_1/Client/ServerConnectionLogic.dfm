object serverConLogicForm: TserverConLogicForm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 467
  object SQLConnection: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'DriverName=DataSnap'
      'HostName=localhost'
      'port=211'
      'Filters={}')
    Connected = True
    Left = 176
    Top = 16
    UniqueId = '{BFE7DBF1-8D64-4B6E-9694-213E87D39193}'
  end
  object cdsProduct: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspProduct'
    RemoteServer = dsProviderConnection
    Left = 56
    Top = 152
    object cdsProductID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsProductNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 280
    end
    object cdsProductNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 16000
    end
    object cdsProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Required = True
      Size = 1
    end
    object cdsProductPRICE: TSingleField
      FieldName = 'PRICE'
      Required = True
    end
    object cdsProductSTICKER: TIntegerField
      FieldName = 'STICKER'
    end
  end
  object cdsOrder: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspOrder'
    RemoteServer = dsProviderConnection
    Left = 152
    Top = 152
    object cdsOrderID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsOrderCREATE_DATETIME: TSQLTimeStampField
      FieldName = 'CREATE_DATETIME'
      Required = True
    end
    object cdsOrderORDER_DATE: TDateField
      FieldName = 'ORDER_DATE'
    end
    object cdsOrderORDER_TIME: TTimeField
      FieldName = 'ORDER_TIME'
    end
    object cdsOrderPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object cdsOrderPRICE: TSingleField
      FieldName = 'PRICE'
    end
    object cdsOrderFIO: TWideStringField
      FieldName = 'FIO'
      Size = 280
    end
    object cdsOrderPHONE: TWideStringField
      FieldName = 'PHONE'
      Size = 80
    end
    object cdsOrderADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Size = 1020
    end
    object cdsOrderNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 16000
    end
    object cdsOrderSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
  end
  object cdsRestaurant: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRestaurant'
    RemoteServer = dsProviderConnection
    Left = 264
    Top = 152
    object cdsRestaurantID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsRestaurantNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 400
    end
    object cdsRestaurantADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Size = 1020
    end
    object cdsRestaurantHOURS: TWideStringField
      FieldName = 'HOURS'
      Size = 1020
    end
    object cdsRestaurantMAP: TBlobField
      FieldName = 'MAP'
      Size = 1
    end
  end
  object dsProviderConnection: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection
    Left = 176
    Top = 80
  end
  object cdsFeedback: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFeedback'
    RemoteServer = dsProviderConnection
    Left = 368
    Top = 152
    object cdsFeedbackID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsFeedbackFIO: TWideStringField
      FieldName = 'FIO'
      Size = 280
    end
    object cdsFeedbackPHONE: TWideStringField
      FieldName = 'PHONE'
      Size = 80
    end
    object cdsFeedbackE_MAIL: TWideStringField
      FieldName = 'E_MAIL'
      Size = 160
    end
    object cdsFeedbackNOTES: TWideStringField
      FieldName = 'NOTES'
      Required = True
      Size = 16000
    end
    object cdsFeedbackRATING: TIntegerField
      FieldName = 'RATING'
      Required = True
    end
  end
end
