//---------------------------------------------------------------------------

#ifndef ClientFormMainH
#define ClientFormMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include "ServerConnectionLogic.h"
#include "unitFramePizzaElement.h"
#include <FMX.Objects.hpp>
#include <FMX.DateTimeCtrls.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include "unitFrameRestaurant.h"
#include "unitFrameFeedback.h"
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tcMain;
	TTabItem *tiMainPage;
	TTabItem *tiAllPizzaPage;
	TTabItem *tiPizzaItemPage;
	TTabItem *tiAllRestaurantPage;
	TTabItem *tiTermsPage;
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TLabel *Label1;
	TButton *buttonToFeedbackPage;
	TButton *buttonToContactsPage;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buttonToPizzaPage;
	TButton *buttonToRestaurantPage;
	TButton *buttonToTermsPage;
	TTabItem *tiAllFeedbackPage;
	TTabItem *tiContactsPage;
	TToolBar *ToolBar3;
	TLabel *Label2;
	TToolBar *ToolBar4;
	TLabel *labelPizzaItemCaption;
	TToolBar *ToolBar5;
	TLabel *Label4;
	TToolBar *ToolBar6;
	TLabel *Label5;
	TToolBar *ToolBar7;
	TLabel *Label6;
	TToolBar *ToolBar8;
	TLabel *Label7;
	TScrollBox *ScrollBox1;
	TGridLayout *glList;
	TButton *buttonGoHome1;
	TButton *buttonGoToMenu;
	TLayout *Layout1;
	TImage *imagePizzaItem;
	TLayout *Layout2;
	TScrollBox *ScrollBox2;
	TText *textPizzaItemDescription;
	TLabel *labelPizzaItemPrice;
	TButton *buttonToOrderCreatePage;
	TTabItem *tiMakeOrderPage;
	TToolBar *ToolBar9;
	TLabel *Label3;
	TLayout *Layout3;
	TLabel *labelPizzaOrderName;
	TLabel *labelPizzaOrderPrice;
	TLabel *Label8;
	TMemo *memoOrderAddress;
	TLayout *Layout4;
	TLabel *Label9;
	TEdit *editOrderFIO;
	TLayout *Layout5;
	TLabel *Label10;
	TEdit *editOrderPhone;
	TLayout *Layout6;
	TLabel *Label11;
	TDateEdit *dataEditOrderDate;
	TLayout *Layout7;
	TLabel *Label12;
	TTimeEdit *timeEditOrderTime;
	TLayout *Layout8;
	TLabel *Label13;
	TMemo *memoOrderNotes;
	TLayout *Layout9;
	TButton *buttonOrderConfirmation;
	TButton *buttonGoHome2;
	TScrollBox *ScrollBox3;
	TGridLayout *glListRestaurant;
	TTabItem *tiRestaurantItemPage;
	TToolBar *ToolBar10;
	TLabel *labelRestaurantCaption;
	TButton *Button1;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label14;
	TLabel *Label15;
	TText *textRestaurantAddress;
	TText *textRestaurantHours;
	TPanel *Panel1;
	TImage *imageRestaurantItem;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TTabItem *tiMakeFeedbackPage;
	TToolBar *ToolBar11;
	TLabel *Label16;
	TLayout *Layout10;
	TButton *buttonFeedbackConfirmation;
	TLayout *Layout11;
	TLabel *Label17;
	TMemo *memoFedbackNote;
	TLayout *Layout12;
	TLabel *Label18;
	TLayout *Layout13;
	TLabel *Label19;
	TLayout *Layout14;
	TLabel *Label20;
	TEdit *editFeedbackPhone;
	TLayout *Layout15;
	TLabel *Label21;
	TEdit *editFeedbackFIO;
	TEdit *editFeedbackMail;
	TRadioButton *RadioButton1;
	TRadioButton *RadioButton2;
	TRadioButton *RadioButton3;
	TRadioButton *RadioButton4;
	TRadioButton *RadioButton5;
	TScrollBox *ScrollBox4;
	TGridLayout *glListFeedback;
	TButton *buttonToFeedbackCreatePage;
	TButton *buttonGoHome3;
	TButton *Button2;
	TButton *buttonBackToAllFeedback;
	TButton *buttonGoHome5;
	TButton *buttonGoHome4;
	TMemo *Memo1;
	TMemo *Memo2;
	TLayout *Layout16;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buttonToPizzaPageClick(TObject *Sender);
	void __fastcall buttonToRestaurantPageClick(TObject *Sender);
	void __fastcall buttonToTermsPageClick(TObject *Sender);
	void __fastcall buttonToFeedbackPageClick(TObject *Sender);
	void __fastcall buttonToContactsPageClick(TObject *Sender);
	void __fastcall glListResize(TObject *Sender);
	void __fastcall buttonGoHomeClick(TObject *Sender);
	void __fastcall ToPizzaItemPageClick(TObject *Sender);
	void __fastcall ToRestaurantItemPageClick(TObject *Sender);
	void __fastcall buttonToOrderCreatePageClick(TObject *Sender);
	void __fastcall buttonOrderConfirmationClick(TObject *Sender);
	void __fastcall RadioButtonChange(TObject *Sender);
	void __fastcall glListFeedbackResize(TObject *Sender);
	void __fastcall buttonToFeedbackCreatePageClick(TObject *Sender);
	void __fastcall buttonFeedbackConfirmationClick(TObject *Sender);
	void __fastcall buttonBackToAllFeedbackClick(TObject *Sender);
private:	// User declarations

            int myRating;

public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
