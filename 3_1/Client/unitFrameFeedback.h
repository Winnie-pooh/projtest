//---------------------------------------------------------------------------

#ifndef unitFrameFeedbackH
#define unitFrameFeedbackH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TframeFeedback : public TFrame
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TLabel *labelFeedbackItemFIO;
	TLayout *Layout1;
	TLabel *labelFeedbackItemRating;
	TMemo *memoFeedbackItemNote;
private:	// User declarations
public:		// User declarations
	__fastcall TframeFeedback(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TframeFeedback *frameFeedback;
//---------------------------------------------------------------------------
#endif
