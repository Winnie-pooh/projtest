//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "ServerConnectionLogic.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TserverConLogicForm *serverConLogicForm;
//---------------------------------------------------------------------------
__fastcall TserverConLogicForm::TserverConLogicForm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall TserverConLogicForm::~TserverConLogicForm()
{
	delete FServerMethods1Client;
}

TServerMethods1Client* TserverConLogicForm::GetServerMethods1Client(void)
{
	if (FServerMethods1Client == NULL)
	{
		SQLConnection->Open();
		FServerMethods1Client = new TServerMethods1Client(SQLConnection->DBXConnection, FInstanceOwner);
	}
	return FServerMethods1Client;
};

void __fastcall TserverConLogicForm::DataModuleCreate(TObject *Sender)
{
	SQLConnection	->Open();
	cdsOrder		->Open();
	cdsProduct		->Open();
	cdsRestaurant	->Open();
    cdsFeedback     ->Open();
}
//---------------------------------------------------------------------------

