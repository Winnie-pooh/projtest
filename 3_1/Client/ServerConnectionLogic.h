//----------------------------------------------------------------------------

#ifndef ServerConnectionLogicH
#define ServerConnectionLogicH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class TserverConLogicForm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection;
	TClientDataSet *cdsProduct;
	TClientDataSet *cdsOrder;
	TClientDataSet *cdsRestaurant;
	TDSProviderConnection *dsProviderConnection;
	TIntegerField *cdsOrderID;
	TSQLTimeStampField *cdsOrderCREATE_DATETIME;
	TDateField *cdsOrderORDER_DATE;
	TTimeField *cdsOrderORDER_TIME;
	TIntegerField *cdsOrderPRODUCT_ID;
	TSingleField *cdsOrderPRICE;
	TWideStringField *cdsOrderFIO;
	TWideStringField *cdsOrderPHONE;
	TWideStringField *cdsOrderADDRESS;
	TWideStringField *cdsOrderNOTES;
	TIntegerField *cdsOrderSTATUS;
	TIntegerField *cdsProductID;
	TWideStringField *cdsProductNAME;
	TWideStringField *cdsProductNOTES;
	TBlobField *cdsProductIMAGE;
	TSingleField *cdsProductPRICE;
	TIntegerField *cdsProductSTICKER;
	TIntegerField *cdsRestaurantID;
	TWideStringField *cdsRestaurantNAME;
	TWideStringField *cdsRestaurantADDRESS;
	TWideStringField *cdsRestaurantHOURS;
	TBlobField *cdsRestaurantMAP;
	TClientDataSet *cdsFeedback;
	TIntegerField *cdsFeedbackID;
	TWideStringField *cdsFeedbackFIO;
	TWideStringField *cdsFeedbackPHONE;
	TWideStringField *cdsFeedbackE_MAIL;
	TWideStringField *cdsFeedbackNOTES;
	TIntegerField *cdsFeedbackRATING;
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TServerMethods1Client* FServerMethods1Client;
	TServerMethods1Client* GetServerMethods1Client(void);
public:		// User declarations
	__fastcall TserverConLogicForm(TComponent* Owner);
	__fastcall ~TserverConLogicForm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TServerMethods1Client* ServerMethods1Client = {read=GetServerMethods1Client, write=FServerMethods1Client};
};
//---------------------------------------------------------------------------
extern PACKAGE TserverConLogicForm *serverConLogicForm;
//---------------------------------------------------------------------------
#endif
