//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TformMain::FormSaveState(TObject *Sender)
{
	TBinaryWriter* x;
	SaveState->Stream->Clear();
	x = new TBinaryWriter(SaveState->Stream);
	try{
		x->Write(tabcontrolMain->ActiveTab->Index);
		x->Write(editOne->Text);
		x->Write(dataeditTwo->Date.Val);
		x->Write(spinboxMain->Value);
	}
	__finally{
		x->DisposeOf();
    }
}
//---------------------------------------------------------------------------
void __fastcall TformMain::FormCreate(TObject *Sender)
{
	TBinaryReader* x;
	if(SaveState->Stream->Size>0)
	{
		x = new TBinaryReader(SaveState->Stream, TEncoding::UTF8, false);
		try{
			tabcontrolMain->ActiveTab = tabcontrolMain->Tabs[x->ReadInteger()];
			editOne->Text = x->ReadString();
			dataeditTwo->Date = x->ReadDouble();
			spinboxMain->Value = x->ReadSingle();
		}
		__finally{
			x->DisposeOf();
		}
	}
}
//---------------------------------------------------------------------------
