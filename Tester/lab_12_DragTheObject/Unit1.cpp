//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TformMain::MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, float X, float Y)
{
	Fx=X;
	Fy=Y;
	IsDragging = true;
	((TControl *)Sender)->BringToFront();
	((TControl *)Sender)->Root->Captured = interface_cast<IControl>(Sender);

	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color=TAlphaColorRec::Lightblue;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::MouseMove(TObject *Sender, TShiftState Shift, float X,
          float Y)
{
	if (IsDragging && Shift.Contains(ssLeft))
	{
        // Bottom and Right Breaker;
		if ((((TControl *)Sender)->Position->X+X-Fx+((TControl *)Sender)->Width)>=formMain->Width)
			return;
		if ((((TControl *)Sender)->Position->Y+Y-Fy+((TControl *)Sender)->Height)>=formMain->Height)
			return;

		// Top and Left Breaker;
		if ((((TControl *)Sender)->Position->X+X-Fx)<=0)
			return;
		if ((((TControl *)Sender)->Position->Y+Y-Fy)<=0)
			return;


		((TControl *)Sender)->Position->X+=X-Fx;
		((TControl *)Sender)->Position->Y+=Y-Fy;
        if (dynamic_cast<TShape*>(Sender))
			((TShape *)Sender)->Fill->Color=TAlphaColorRec::Lightpink;
	}



}
//---------------------------------------------------------------------------
void __fastcall TformMain::MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y)
{
	IsDragging=false;
	((TControl *)Sender)->Root->Captured = 0;

	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color=TAlphaColorRec::Yellow;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::MouseEnter(TObject *Sender)
{
	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color=TAlphaColorRec::Lightgrey;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::MouseLeave(TObject *Sender)
{
	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color=TAlphaColorRec::Lightgreen;
}
//---------------------------------------------------------------------------
