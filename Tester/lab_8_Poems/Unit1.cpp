//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

inline int Low(const System::UnicodeString &)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return 1;
	#else
		return 0;
	#endif
}

inline int High(const System::UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return S.Length();
	#else
		return S.Length()-1;
	#endif
}

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	System::UnicodeString x;

	for(int i=0; i<memoMMain->Lines->Count; i++)
	{
		x=memoMMain->Lines->Strings[i];
		if(i%2==1)
		{
			for(int j=Low(x); j<=High(x); j++)
			{
				if (x[j]!=' ')
					x[j] = 'x';
			}
		}
		memoMCodeOneByOne->Lines->Add(x);
	}

	bool xFlag;
	for(int i=0; i<memoMMain->Lines->Count; i++)
	{
		x=memoMMain->Lines->Strings[i];
		xFlag=false;
		for(int j=Low(x); j<=High(x); j++)
		{
			if((xFlag)&&(x[j]!= ' '))
				x[j] =  'x';
			if(!xFlag && (x[j]==' '))
				xFlag=true;
		}
		memoMCodeFirst->Lines->Add(x);
    }

}
//---------------------------------------------------------------------------
