//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.NumberBox.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout1;
	TRectangle *Rectangle1;
	TLabel *labelCorrect;
	TRectangle *Rectangle2;
	TLabel *labelUnCorrect;
	TLabel *labelWhat;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buttonYes;
	TButton *buttonNo;
	TButton *buttonReset;
	TNumberBox *numberboxRange;
	TLabel *Label1;
	TCheckBox *checkboxIvent;
	void __fastcall buttonResetClick(TObject *Sender);
	void __fastcall buttonYesClick(TObject *Sender);
	void __fastcall buttonNoClick(TObject *Sender);
private:	// User declarations
	int AnsCorrect, AnsUnCorrect, SymbolNumber;
	float What1;
	float What2;
	float WhatAnswer;
	void CreateWhat();
	void Answer(bool valueAnswer);

	char *Symbols;               //System::UnicodeString

public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
