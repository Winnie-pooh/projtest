//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	Symbols="+-*/";
	Randomize();
	CreateWhat();
	AnsCorrect = 0;
	AnsUnCorrect  = 0;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonResetClick(TObject *Sender)
{
	labelCorrect->Text="Correct";
	labelUnCorrect->Text="UnCorrect";
	CreateWhat();
}
//---------------------------------------------------------------------------



void __fastcall TformMain::buttonYesClick(TObject *Sender)
{
	Answer(true);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonNoClick(TObject *Sender)
{
	Answer(false);
}
//---------------------------------------------------------------------------

void TformMain::CreateWhat()
{
	if (checkboxIvent->IsChecked)
		SymbolNumber=Random(4);
	SymbolNumber=Random(2);


	What1 = Random(StrToIntDef(numberboxRange->Text, 100));
	What2 = Random(StrToIntDef(numberboxRange->Text, 100));

	int ForAnswer = Random(100);
	if (ForAnswer<45)
	{
		if (SymbolNumber==0)
			WhatAnswer = What1+What2;
		if (SymbolNumber==1)
			WhatAnswer = What1-What2;
		if (SymbolNumber==2)
			WhatAnswer = What1*What2;
		if (SymbolNumber==3)
			WhatAnswer = What1/What2;
	}
	else
	{
		if (SymbolNumber==0)
			WhatAnswer = Random(StrToIntDef(numberboxRange->Text, 100)*2);
		if (SymbolNumber==1)
			WhatAnswer = Random(StrToIntDef(numberboxRange->Text, 100)*3)-StrToIntDef(numberboxRange->Text, 100);
		if (SymbolNumber==2)
			WhatAnswer = Random(StrToIntDef(numberboxRange->Text, 100)*StrToIntDef(numberboxRange->Text, 100));
		if (SymbolNumber==3) 
			WhatAnswer = (float)Random(StrToIntDef(numberboxRange->Text, 100)*StrToIntDef(numberboxRange->Text, 100))/
			(float)Random(StrToIntDef(numberboxRange->Text, 100)*StrToIntDef(numberboxRange->Text, 100));	
	}


	labelWhat->Text = FloatToStr(What1)+" "+Symbols[SymbolNumber]+" "+FloatToStr(What2)+" = "+FloatToStr(WhatAnswer);

}
void TformMain::Answer(bool valueAnswer)
{
	float raven;
	if (SymbolNumber==0) 
		raven = What1+What2;
	if (SymbolNumber==1) 
		raven = What1-What2;
	if (SymbolNumber==2) 
		raven = What1*What2;
	if (SymbolNumber==3) 
		raven = What1/What2;
		
	if(WhatAnswer==raven)
	{
		if (valueAnswer) {
			AnsCorrect++;
			labelCorrect->Text="Correct "+IntToStr(AnsCorrect);
		}
		else
		{
			AnsUnCorrect++;
			labelUnCorrect->Text="UnCorrect "+IntToStr(AnsUnCorrect);
		}
	}
	else
	{
			if (!valueAnswer) {
			AnsCorrect++;
			labelCorrect->Text="Correct "+IntToStr(AnsCorrect);
		}
		else
		{
			AnsUnCorrect++;
			labelUnCorrect->Text="UnCorrect "+IntToStr(AnsUnCorrect);
		}
	}
	CreateWhat();

}