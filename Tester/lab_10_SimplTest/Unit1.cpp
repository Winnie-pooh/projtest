//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	TabItem1->IsSelected = true;
}
//---------------------------------------------------------------------------


void __fastcall TformMain::button11Click(TObject *Sender)
{
	int tag = ((TControl *)Sender)->Tag;
	System::UnicodeString questNumb = ((TControl *)Sender)->Name[7];

	if (tag==1)
		memoRezults->Lines->Add( "������ "+questNumb+" - �����");
	else
		memoRezults->Lines->Add( "������ "+questNumb+" - �������");


	TComponent* TabItem;

	for(int i=1; i<8; i++)
	{
		TabItem = formMain->FindComponent("TabItem"+IntToStr(i));

		if(TabItem)
		{
			if (((TTabItem*)TabItem)->IsSelected)
			{
				((TTabItem*)TabItem)->Enabled=false;

				TabItem = formMain->FindComponent("TabItem"+IntToStr(i+1));
				((TTabItem*)TabItem)->IsSelected = true;
				((TTabItem*)TabItem)->Enabled=true;
				break;
			}
		}
	}


}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonResetClick(TObject *Sender)
{
	memoRezults->Lines->Clear();

    TComponent* TabItem;

	for(int i=1; i<8; i++)
	{
		TabItem = formMain->FindComponent("TabItem"+IntToStr(i));

		if(TabItem)
		{
			if (((TTabItem*)TabItem)->Enabled)
			{
				((TTabItem*)TabItem)->Enabled=false;

				TabItem = formMain->FindComponent("TabItem1");
				((TTabItem*)TabItem)->IsSelected = true;
				((TTabItem*)TabItem)->Enabled=true;
				break;
			}
		}
	}

}
//---------------------------------------------------------------------------

