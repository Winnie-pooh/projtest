//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TLabel *Label1;
	TButton *button11;
	TButton *button14;
	TButton *button13;
	TButton *button12;
	TTabItem *TabItem6;
	TMemo *memoRezults;
	TButton *button21;
	TButton *button22;
	TButton *button23;
	TButton *button24;
	TLabel *Label2;
	TButton *Button31;
	TButton *Button32;
	TButton *Button33;
	TButton *Button34;
	TLabel *Label3;
	TTabItem *TabItem4;
	TButton *Button41;
	TButton *Button42;
	TButton *Button43;
	TButton *Button44;
	TLabel *Label4;
	TTabItem *TabItem5;
	TButton *Button51;
	TButton *Button52;
	TButton *Button53;
	TButton *Button54;
	TLabel *Label5;
	TButton *buttonReset;
	void __fastcall button11Click(TObject *Sender);
	void __fastcall buttonResetClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
