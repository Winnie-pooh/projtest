//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TformMain::TakePhotoFromLibraryAction1DidFinishTaking(TBitmap *Image)

{
	imageMain->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::TakePhotoFromCameraAction1DidFinishTaking(TBitmap *Image)

{
	imageMain->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::ShowShareSheetAction1BeforeExecute(TObject *Sender)
{
	ShowShareSheetAction1->Bitmap->Assign(imageMain->Bitmap);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::acClearExecute(TObject *Sender)
{
    imageMain->Bitmap->SetSize(0,0);
}
//---------------------------------------------------------------------------
