//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Filter.Effects.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TShadowEffect *ShadowEffect1;
	TBlurEffect *BlurEffect1;
	TShadowEffect *ShadowEffect2;
	TInnerGlowEffect *InnerGlowEffect1;
	TBandsEffect *BandsEffect1;
	TFillEffect *FillEffect1;
	TTilerEffect *TilerEffect1;
	TWrapEffect *WrapEffect1;
	TSepiaEffect *SepiaEffect1;
	TMonochromeEffect *MonochromeEffect1;
	TContrastEffect *ContrastEffect1;
	TTilerEffect *TilerEffect2;
	TBlurEffect *BlurEffect2;
private:	// User declarations
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
