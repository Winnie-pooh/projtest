//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Layouts.hpp>
#include <FMXTee.Chart.hpp>
#include <FMXTee.Engine.hpp>
#include <FMXTee.Procs.hpp>
#include <FMXTee.Series.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *Schedule;
	TMediaPlayer *MediaPlayer1;
	TMediaPlayerControl *MediaPlayerControl1;
	TButton *buttonVideoPlayPause;
	TLayout *Layout1;
	TButton *buttonVideoPlayStop;
	TChart *Chart1;
	TGridPanelLayout *GridPanelLayout1;
	TLayout *Layout2;
	TButton *Button1;
	TLineSeries *Series1;
	TButton *Clear;
	TTabItem *TabItem2;
	TChart *Chart2;
	TLineSeries *Series2;
	TButton *Button2;
	TImage *img;
	TButton *Button3;
	void __fastcall buttonVideoPlayPauseClick(TObject *Sender);
	void __fastcall buttonVideoPlayStopClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ClearClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
				int PauseTime;
                int x, y;
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
