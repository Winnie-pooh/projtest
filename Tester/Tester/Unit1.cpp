//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	PauseTime = 0;
	try{
        MediaPlayer1->FileName = "Res\\Zedd Ignite.avi";
	}
	__finally{
		//ShowMessage("Bad Video File");
		x=0;y=0;
	}
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonVideoPlayPauseClick(TObject *Sender)
{
		TButton* baton = (TButton*)Sender;
		try{


			if (baton->Tag==0) {
				MediaPlayer1->Media->CurrentTime=PauseTime;
				MediaPlayer1->Play();
				baton->Text="Pause";
				baton->Tag=1;
			}
			else{
				PauseTime = MediaPlayer1->Media->CurrentTime;
				MediaPlayer1->Stop();
				baton->Text= "Play and Pause";
				baton->Tag=0;
			}
		}
		__finally{
			//baton->DisposeOf();
		}
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonVideoPlayStopClick(TObject *Sender)
{
         TButton* baton = (TButton*)Sender;
		try{


			if (baton->Tag==0) {
				MediaPlayer1->Media->CurrentTime=PauseTime;
				MediaPlayer1->Play();
				baton->Text="Stop";
				baton->Tag=1;
			}
			else{
				PauseTime = 0;
				MediaPlayer1->Stop();
				baton->Text= "Play and Stop";
				baton->Tag=0;
			}
		}
		__finally{
			//baton->DisposeOf();
		}
}
//---------------------------------------------------------------------------


void __fastcall TformMain::Button1Click(TObject *Sender)
{
	//Chart1->Series->Add(100,200,"Red")
	int randomNumber = Random(1001);
	System::UnicodeString str="Text_"+IntToStr(randomNumber);

	x += Random(100);
	y += Random(100)-50;

	Series1->DrawBetweenPoints=true;
	//Series1->Add(randomNumber,str,Random(1001));

	//Series1->Add(x, "", y);




}
//---------------------------------------------------------------------------

void __fastcall TformMain::ClearClick(TObject *Sender)
{
			Series1->Clear();
			Series1->AddNullXY(0, 0);

}
//---------------------------------------------------------------------------

void __fastcall TformMain::Button2Click(TObject *Sender)
{
	Series2->Clear();
			Series2->AddNullXY(0, 0);
			Series2->DrawBetweenPoints=true;
            Series2->AddXY(100, 50);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::Button3Click(TObject *Sender)
{
	img->Bitmap->LoadFromFile("D:\\Program\\Git\\Tester\\Tester\\Res\\ikar.png");
}
//---------------------------------------------------------------------------

