//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TTheForm *TheForm;
//---------------------------------------------------------------------------
__fastcall TTheForm::TTheForm(TComponent* Owner)
	: TForm(Owner)
{
	flag=false;
	is_stoped=false;
}

//---------------------------------------------------------------------------
void __fastcall TTheForm::TimerTimer(TObject *Sender)
{
	char time[2];
	if (!is_stoped)
	{
		time[0]=TimeToStr(start_time-Now())[6];
		time[1]=TimeToStr(start_time-Now())[7];
		labelSec->Text = time;

		time[0]=TimeToStr(start_time-Now())[3];
		time[1]=TimeToStr(start_time-Now())[4];
		labelMin->Text = time;

		//time[0]=;
		if (true) {
		//TODO[0]
		}
		labelHour->Text = TimeToStr(start_time-Now())[1];
	}
	else
	{
		time[0]=TimeToStr(start_time-stop_time_1+stop_time_2-Now())[6];
		time[1]=TimeToStr(start_time-stop_time_1+stop_time_2-Now())[7];
		labelSec->Text = time;

		time[0]=TimeToStr(start_time-stop_time_1+stop_time_2-Now())[3];
		time[1]=TimeToStr(start_time-stop_time_1+stop_time_2-Now())[4];
		labelMin->Text = time;

		//time[0]=;
		if (true) {
		//TODO[0]
		}
		labelHour->Text = TimeToStr(start_time-stop_time_1+stop_time_2-Now())[1];
    }

}
//---------------------------------------------------------------------------
void __fastcall TTheForm::butStartClick(TObject *Sender)
{
	if (!flag)
	{
		start_time = Now();
		Timer->Enabled = true;
	}

	flag=true;
}
//---------------------------------------------------------------------------

void __fastcall TTheForm::butStopClick(TObject *Sender)
{
	stop_time_1=Now();
	Timer->Enabled = false;
	is_stoped=true;
}
//---------------------------------------------------------------------------

void __fastcall TTheForm::butContinueClick(TObject *Sender)
{
	if (flag) {
		stop_time_2=Now();
		Timer->Enabled = true;
	}

}
//---------------------------------------------------------------------------

void __fastcall TTheForm::butClearClick(TObject *Sender)
{
    start_time=Now();
	labelSec->Text = "00";
	labelMin->Text = "00";
	labelHour->Text = "0";
	flag=false;
	is_stoped=false;
}
//---------------------------------------------------------------------------

