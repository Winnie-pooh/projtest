//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TTheForm : public TForm
{
__published:	// IDE-managed Components
	TTimer *Timer;
	TButton *butStart;
	TLabel *labelSec;
	TLabel *labelMin;
	TLabel *labelHour;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TButton *butStop;
	TButton *butContinue;
	TButton *butClear;
	void __fastcall TimerTimer(TObject *Sender);
	void __fastcall butStartClick(TObject *Sender);
	void __fastcall butStopClick(TObject *Sender);
	void __fastcall butContinueClick(TObject *Sender);
	void __fastcall butClearClick(TObject *Sender);
private:	// User declarations
	TDateTime start_time, stop_time_1, stop_time_2;
public:		// User declarations
    bool flag, is_stoped;
	__fastcall TTheForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTheForm *TheForm;
//---------------------------------------------------------------------------
#endif
