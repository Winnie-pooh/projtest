//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;

typedef System::UnicodeString string;

//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	tabcontrolMain->ActiveTab = tabitemMain;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonGoOutClick(TObject *Sender)
{
    formMain->Close();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonHelpClick(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemHelp;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::GoToMenuClick(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemMain;
    timerMain->Enabled=false;
}
//---------------------------------------------------------------------------



void __fastcall TformMain::StartClick(TObject *Sender)
{
	Correct = 0;
	UnCorrect = 0;

	tabcontrolMain->ActiveTab = tabitemGame;
	timerMain->Enabled=true;

	TimeValue = Time().Val + (float)30/(24*60*60);       //30 sec


	CreateTheQuestion();

}
//---------------------------------------------------------------------------

void __fastcall TformMain::MainTimer(TObject *Sender)
{
	double x = TimeValue - Time().Val;
	labelTime->Text = FormatDateTime("nn:ss", x);

	if (x>=0)
		return;

	timerMain->Enabled = false;
	labelGood->Text = Format(L"Goodes = %d", ARRAYOFCONST((Correct)));
	labelBad->Text = Format(L"Badles = %d", ARRAYOFCONST((UnCorrect)));

	tabcontrolMain->ActiveTab = tabitemRezult;
}
//---------------------------------------------------------------------------

void TformMain::CreateTheQuestion()
{
	const string c1="02468", c2="13579", c3="������Ũ��", c4="���������������������";

	labelPoints->Text = Format(L"Points: %d", ARRAYOFCONST((Correct)));

	bool Even=(Random(2)==1), Vowel=(Random(2)==1), firstBox=(Random(2)==1);
	string Question = "";

	if (Even)
		Question+=c1.SubString0(Random(c1.Length()), 1);
	else
		Question+=c2.SubString0(Random(c2.Length()), 1);

	if (Vowel)
		Question+=c3.SubString0(Random(c3.Length()), 1);
	else
		Question+=c4.SubString0(Random(c4.Length()), 1);

	if (firstBox){
		Answer = Even;
		labelEven->Text = Question;
		labelVowel->Text = "";
	}
	else
	{
		Answer = Vowel;
		labelEven->Text = "";
		labelVowel->Text = Question;
    }
}

void __fastcall TformMain::YesClick(TObject *Sender)
{
	if (Answer==true)
		Correct++;
	else
		UnCorrect++;
	CreateTheQuestion();

}
//---------------------------------------------------------------------------

void __fastcall TformMain::NoClick(TObject *Sender)
{
	if (Answer==false)
		Correct++;
	else
		UnCorrect++;
	CreateTheQuestion();
}
//---------------------------------------------------------------------------

