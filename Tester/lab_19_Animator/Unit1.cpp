//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button1Click(TObject *Sender)
{
	Label1_1->Position->X = -Label1_1->Width;
	Label1_2->Position->X = this->Width+Label1_2->Width;
	Label1_3->Position->X = -Label1_3->Width;

	TAnimator::AnimateIntWait(Label1_1, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label1_2, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label1_3, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button2Click(TObject *Sender)
{
	Label2_1->AutoSize = true;
	Label2_1->TextSettings->Font->Size = 50000;

	TAnimator::AnimateIntWait(Label2_1, "TextSettings.Font.Size", 40, 2, TAnimationType::Out, TInterpolationType::Linear);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button6Click(TObject *Sender)
{
	int y = GridPanelLayout1->Position->Y;
	GridPanelLayout1->Position->Y = -GridPanelLayout1->Height;
	TAnimator::AnimateInt(GridPanelLayout1, "Position.Y", y, 1.5, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------


