//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>

#include <System.IOUtils.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLayout *Layout2;
	TLayout *Layout3;
	TLayout *Layout4;
	TListView *listviewMain;
	TMediaPlayer *mediaplayerMain;
	TTimer *timerMain;
	TTrackBar *trackbarMain;
	TLabel *labelDuration;
	TLabel *labelCurrentTime;
	TButton *buttonPlay;
	TButton *buttonNext;
	TButton *buttonPrev;
	TButton *buttonStop;
	TButton *buttonPause;
	TLabel *labelPath;
	TLabel *Label4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall listviewMainItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buttonPlayClick(TObject *Sender);
	void __fastcall buttonPauseClick(TObject *Sender);
	void __fastcall buttonStopClick(TObject *Sender);
	void __fastcall trackbarMainChange(TObject *Sender);
	void __fastcall timerMainTimer(TObject *Sender);
	void __fastcall buttonPrevClick(TObject *Sender);
	void __fastcall buttonNextClick(TObject *Sender);

private:	// User declarations
			unsigned int FPauseTime;
			void DoRefreshList(UnicodeString aPath);
			void DoStop();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
