//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"

#include <System.IOUtils.hpp>

TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TForm1::DoRefreshList(UnicodeString aPath)
{
	labelPath->Text = aPath;
	DynamicArray<UnicodeString> x;
	TListViewItem *xItem;
	x = System::Ioutils::TDirectory::GetFiles(aPath, "*.mp3");
	listviewMain->BeginUpdate();
	try{
		for (int i = 0; i < x.Length; i++)
		{
			xItem = listviewMain->Items->Add();
			xItem->Text = System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
			xItem->Detail = x[i];
		}
	}
	__finally{
		listviewMain->EndUpdate();
	}
}

void TForm1::DoStop()
{
	mediaplayerMain->Stop();
	timerMain->Enabled = false;
	listviewMain->ItemIndex = -1;
	trackbarMain->Value = 0;
    FPauseTime = 0;
}

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FPauseTime = 0;
	DoRefreshList(System::Ioutils::TPath::GetSharedMusicPath());
}
//---------------------------------------------------------------------------

void __fastcall TForm1::listviewMainItemClick(TObject * const Sender, TListViewItem * const AItem)
{
    buttonPlayClick(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonPlayClick(TObject *Sender)
{
	if(listviewMain->Index==(-1))
		return;

	timerMain->Enabled = false;
	mediaplayerMain->Clear();
	mediaplayerMain->FileName = listviewMain->Items->AppearanceItem[listviewMain->ItemIndex]->Detail;
	trackbarMain->Enabled = true;
	trackbarMain->Max = mediaplayerMain->Duration;
	labelDuration->Text = Format("%2.2d:%2.2d", ARRAYOFCONST((
								(unsigned int)mediaplayerMain->Duration/MediaTimeScale/60   //min
								,(unsigned int)mediaplayerMain->Duration/MediaTimeScale%60   //sec
	)));
	mediaplayerMain->CurrentTime = FPauseTime;
	FPauseTime = 0;
	mediaplayerMain->Play();
    timerMain->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonPauseClick(TObject *Sender)
{
	if (mediaplayerMain->State == TMediaState::Playing)
	{
		FPauseTime = mediaplayerMain->Media->CurrentTime;
		mediaplayerMain->Stop();
	}
	else
	{
        buttonPlayClick(this);
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonStopClick(TObject *Sender)
{
	if (mediaplayerMain->State == TMediaState::Playing)
	{
        DoStop();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::trackbarMainChange(TObject *Sender)
{
	if (trackbarMain->Tag ==0)
	{
		//�������� �� �����
		if (mediaplayerMain->State == TMediaState::Stopped &&(FPauseTime !=0))
		{
			FPauseTime = trackbarMain->Value;
		}
		else
		{
			mediaplayerMain->CurrentTime = trackbarMain->Value;
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::timerMainTimer(TObject *Sender)
{
	trackbarMain->Tag = 1;
	trackbarMain->Value =  mediaplayerMain->CurrentTime;
	trackbarMain->Tag = 0;
	labelCurrentTime->Text =  Format("%2.2d:%2.2d", ARRAYOFCONST((
								(unsigned int)mediaplayerMain->CurrentTime/MediaTimeScale/60   //min
								,(unsigned int)mediaplayerMain->CurrentTime/MediaTimeScale%60   //sec
	)));
	//
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonPrevClick(TObject *Sender)
{
	if(listviewMain->Index !=-1 && listviewMain->ItemIndex>0)
	{
		listviewMain->ItemIndex = listviewMain->ItemIndex -1;
		FPauseTime = 0;
		buttonPlayClick(this);
	}
	else{
		DoStop();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonNextClick(TObject *Sender)
{
	if(listviewMain->Index !=-1 && listviewMain->ItemIndex < listviewMain->ItemCount-1)
	{
		listviewMain->ItemIndex = listviewMain->ItemIndex +1;
		FPauseTime = 0;
		buttonPlayClick(this);
	}
	else{
		DoStop();
	}
}
//---------------------------------------------------------------------------

