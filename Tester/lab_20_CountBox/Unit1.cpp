//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "unitRandomArrayUnique.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TformMain::FormCreate(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemMenu;


	FListBox = new TList;

	for (int i = 1; i <= cMaxBox; i++) {
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}

	FListAnswer = new TList;
	FListAnswer->Add(buttonAnswer1);
	FListAnswer->Add(buttonAnswer2);
	FListAnswer->Add(buttonAnswer3);
	FListAnswer->Add(buttonAnswer4);
	FListAnswer->Add(buttonAnswer5);
	FListAnswer->Add(buttonAnswer6);

    TBinaryReader* x;
	TComponent* LabelResult;

	if(SaveState->Stream->Size>0)
	{
		x = new TBinaryReader(SaveState->Stream, TEncoding::UTF8, false);
		try{
			for(int i=1; i<=11; i++)
			{
				LabelResult = formMain->FindComponent("labelRes_"+IntToStr(i));
				if(LabelResult)
				{
					((TLabel*)LabelResult)->Text= x->ReadString();
				}
			}
		}
		__finally{
			x->DisposeOf();
			LabelResult->DisposeOf();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TformMain::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------

void TformMain::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val+(double)30/(24*60*60);
	timerMain->Enabled = true;
	DoContinue();
}

void TformMain::DoContinue()
{
	for (int i = 0;  i < cMaxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;
	}

	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);

	for (int i = 0; i<FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Red;
	}

	int xAnswerStart= FNumberCorrect-Random(cMaxAnswer-1);
	if(xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;

	for (int i=0; i < cMaxAnswer; i++) {
		((TButton *)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart+i);
	}
}

void TformMain::DoAnswer(int Value)
{
	(Value == FNumberCorrect)?FCountCorrect++ : FCountWrong++;

	if (FCountWrong >5)
	{
		timerMain->Enabled= false;
		tabcontrolMain->ActiveTab = tabitemBadMen;
		float y = PanelTerrible->Position->Y/2;
		PanelTerrible->Position->Y =-PanelTerrible->Height;
		TAnimator::AnimateInt(PanelTerrible, "Position.Y", y+30, 1.5, TAnimationType::Out, TInterpolationType::Back);
	}

	DoContinue();
}

void TformMain::DoFinish(){
	timerMain->Enabled = false;
	tabcontrolMain->ActiveTab = tabitemScore;
	labelScore->Text = "Your score: "+IntToStr(FCountCorrect);
}
void __fastcall TformMain::buttonAnswer(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall TformMain::timerMainTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	labelTime->Text = FormatDateTime("nn:ss", x);
	if (x<=0) {
		DoFinish();
	}
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonPlayClick(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemGame;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonExitClick(TObject *Sender)
{
	this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonBackToMenue1Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemMenu;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonScoreClick(TObject *Sender)
{
	TComponent* LabelResult;
	int LabelScore=0;
	int old, news;
	bool flag = true;

	for(int i=1; i<=11; i++)
	{
		LabelResult = formMain->FindComponent("labelRes_"+IntToStr(i));
		if(LabelResult)
		{
			LabelScore = StrToIntDef(((TLabel*)LabelResult)->Text, 0);

			if (LabelScore>FCountCorrect)
			{
				continue;
			}
			if (flag)
			{
				old =  StrToIntDef(((TLabel*)LabelResult)->Text, 0);
				((TLabel*)LabelResult)->Text=IntToStr(FCountCorrect);
				flag=!flag;
				continue;
			}

			news = StrToIntDef(((TLabel*)LabelResult)->Text, 0);
			((TLabel*)LabelResult)->Text=IntToStr(old);
			old = news;

		}
	}

	tabcontrolMain->ActiveTab = tabitemBests;

	LabelResult->DisposeOf();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonScoreClearClick(TObject *Sender)
{
	TComponent* LabelResult;

	for(int i=1; i<=11; i++)
	{
		LabelResult = formMain->FindComponent("labelRes_"+IntToStr(i));
		if(LabelResult)
		{
			((TLabel*)LabelResult)->Text=IntToStr(0);
		}
	}
	LabelResult->DisposeOf();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::FormSaveState(TObject *Sender)
{
	TBinaryWriter* x;
	TComponent* LabelResult;

	SaveState->Stream->Clear();
	x = new TBinaryWriter(SaveState->Stream);
	try{
		for(int i=1; i<=11; i++)
		{
			LabelResult = formMain->FindComponent("labelRes_"+IntToStr(i));
			if(LabelResult)
			{
				x->Write(((TLabel*)LabelResult)->Text);
			}
		}
	}
	__finally{
		x->DisposeOf();
		LabelResult->DisposeOf();
	}
}
//---------------------------------------------------------------------------

