//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tabcontrolMain;
	TTabItem *tabitemMenu;
	TTabItem *tabitemScore;
	TTabItem *tabitemBadMen;
	TTabItem *tabitemGame;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buttonAnswer1;
	TButton *buttonAnswer2;
	TButton *buttonAnswer3;
	TButton *buttonAnswer4;
	TButton *buttonAnswer5;
	TButton *buttonAnswer6;
	TLayout *Layout1;
	TButton *buttonReset;
	TLabel *Label1;
	TLabel *labelTime;
	TTimer *timerMain;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TGridPanelLayout *gridpanellayoutMenu;
	TButton *buttonPlay;
	TButton *buttonExit;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TPanel *PanelTerrible;
	TToolBar *ToolBar3;
	TLabel *Label3;
	TGridPanelLayout *GridPanelLayoutTerrible;
	TButton *Button1;
	TButton *Button2;
	TPanel *Panel2;
	TToolBar *ToolBar1;
	TToolBar *ToolBar4;
	TLabel *Label4;
	TLabel *Label5;
	TLayout *Layout3;
	TLabel *labelScore;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button3;
	TButton *Button4;
	TTabItem *tabitemBests;
	TToolBar *ToolBar5;
	TButton *Button5;
	TLabel *Label6;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label7;
	TLabel *labelRes_1;
	TLabel *Label9;
	TLabel *labelRes_2;
	TLabel *Label11;
	TLabel *labelRes_3;
	TLabel *Label13;
	TLabel *labelRes_4;
	TLabel *Label15;
	TLabel *labelRes_5;
	TLabel *Label17;
	TLabel *labelRes_6;
	TLabel *Label19;
	TLabel *labelRes_7;
	TLabel *Label21;
	TLabel *labelRes_8;
	TLabel *Label23;
	TLabel *labelRes_9;
	TLabel *Label25;
	TLabel *labelRes_10;
	TPanel *Panel1;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *Panel8;
	TPanel *Panel9;
	TPanel *Panel10;
	TPanel *Panel11;
	TButton *buttonScore;
	TPanel *Panel12;
	TPanel *Panel13;
	TPanel *Panel14;
	TPanel *Panel15;
	TPanel *Panel16;
	TPanel *Panel17;
	TPanel *Panel18;
	TPanel *Panel19;
	TPanel *Panel20;
	TPanel *Panel21;
	TButton *buttonScoreClear;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buttonAnswer(TObject *Sender);
	void __fastcall timerMainTimer(TObject *Sender);
	void __fastcall buttonPlayClick(TObject *Sender);
	void __fastcall buttonExitClick(TObject *Sender);
	void __fastcall buttonBackToMenue1Click(TObject *Sender);
	void __fastcall buttonScoreClick(TObject *Sender);
	void __fastcall buttonScoreClearClick(TObject *Sender);
	void __fastcall FormSaveState(TObject *Sender);
private:	// User declarations

			int FCountCorrect;
			int FCountWrong;
			int FNumberCorrect;
			double FTimeValue;
			TList *FListBox;
			TList *FListAnswer;
			void DoReset();
			void DoContinue();
			void DoAnswer(int Value);
			void DoFinish();
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};

		const int cMaxBox = 25;
		const int cMaxAnswer = 6;
		const int cMinPossible = 4;
		const int cMaxPossible = 14;

//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
