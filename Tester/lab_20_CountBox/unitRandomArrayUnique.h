//---------------------------------------------------------------------------


#ifndef unitRandomArrayUniqueH
#define unitRandomArrayUniqueH



#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>


int* RandomArrayUnique(int aMax, int aCount);

//---------------------------------------------------------------------------
#endif
