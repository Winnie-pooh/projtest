//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "UnitMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"

#include <stdio.h>
#include <pcreposi.h>
#include <string.h>

typedef System::UnicodeString string;

TformMain *formMain;

inline int Low(const System::UnicodeString &)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return 1;
	#else
		return 0;
	#endif
}

inline int High(const System::UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return S.Length();
	#else
		return S.Length()-1;
	#endif
}

//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	RangeOf=0.0001;

}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------

void __fastcall TformMain::tabcontrolMainChange(TObject *Sender)
{
	if (!tabcontrolMain->ActiveTab->Tag == 0)
		return;

	TComponent* Labels;
	string nameNumb=FloatToStr(RangeOf);




	if(vertScrol->FindComponent("label0t0001")||formMain->FindComponent("label0t0001"))
	{

		for (long double i = RangeOf; ; i+=RangeOf)
		{
			nameNumb=FloatToStr(i);
			for (int k=Low(nameNumb); k<=High(nameNumb); k++)
			{
				if (nameNumb[k]=='.'||nameNumb[k]==',')
				{
					nameNumb[k] ='t';
					break;
				}
			}

			Labels = formMain->FindComponent("label"+nameNumb);
			if(Labels)
				((TLabel*)Labels)->Free();
			else
				break;
		}
	}


	int X=20, Y=20;
	int Width, Posish_old=NULL;
	long double Range = RangeOf;  //RangeOf
	string str="", word="";


	for(int k=0; k<memoMain->Lines->Count; k++)
	{
		str = memoMain->Lines->Strings[k];
		str+=" ";

		for (int i = Low(str) ; i <=High(str); i++)
		{
			word+=str[i];
			//if (!((str[i]>='A')&&(str[i]<='Z')))
			if(str[i]==' ')
			{
				//if (((str[i]>='a')&&(str[i]<='z')))
				{
					//continue;
				}
				Width=(word.Length())*6+2;

				Posish_old = X+Width;

				if (Posish_old>=vertScrol->Width)
				{
					Y+=20;
					X=5;
				}

				Create_Label(Range, X, Y, word, Width);

				X = X+Width;
				Range+=RangeOf;
				word="";
			}


		}
	}


}
//---------------------------------------------------------------------------

void TformMain::Create_Label(long double vRange, int vX, int vY, string vText, int vWidth)
{
	TLabel *label;

	label = new TLabel(this);
	label->Text = vText;
	string nameNumb=FloatToStr(vRange);
			for (int k=Low(nameNumb); k<=High(nameNumb); k++)
			{
				if (nameNumb[k]=='.'||nameNumb[k]==',')
				{
					nameNumb[k] ='t';
					break;
				}
			}
	label->Position->X=vX;
	label->Position->Y=vY;
	//label->TextSettings->Font->Size=20;
	label->Font->Size=18;
	label->Height=20;
	label->Width=vWidth;
	label->HitTest=true;



	label->OnClick = Label1Click;
	label->Name = "label"+nameNumb;
	label->Parent = vertScrol;

}


void __fastcall TformMain::Label1Click(TObject *Sender)
{
	TLabel *label = (TLabel *)Sender;
	string str = label->Text;
	//string str = ((TControl *)Sender)->ToString();
	string url = "https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20161022T203035Z.b23b792244458b61.3a6d6bcb28a8bf50b2a72466844cc0ba4dac517d&text="+str+"&lang=ru";
	//webbrowserMain->Navigate("https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20161022T203035Z.b23b792244458b61.3a6d6bcb28a8bf50b2a72466844cc0ba4dac517d&text="+str+"&lang=ru");

	int i;

	str = IdHTTP1->Get(url);


	string valueStr="", testString="";
	int valueInt, k=0;
	for (i = 50;i<str.Length(); i++) {

		if (valueStr=="<text>")
		{
			if (str[i]=='<')
				break;

			testString+=str[i];
			continue;
		}

		if (str[i]=='<'||str[i]=='t'||str[i]=='e'||str[i]=='x'||str[i]=='t'||str[i]=='>')
		{
			valueStr+=str[i];
			valueInt=i;

			if (valueStr[1]=='>')
				valueStr="";
		}
		else
			valueStr="";

	}

	 ShowMessage(label->Text+" -> "+ testString);

}

//---------------------------------------------------------------------------

void __fastcall TformMain::buttonInfoClick(TObject *Sender)
{
	ShowMessage("��� ����� - ������! \n\n1. ��������� ����� �� ���������� \n2. ������� \n3. �������� �� ���������� �����");
}
//---------------------------------------------------------------------------

