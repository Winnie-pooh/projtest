//---------------------------------------------------------------------------

#ifndef UnitMainH
#define UnitMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <Soap.SOAPHTTPTrans.hpp>
#include <FMX.WebBrowser.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.ExtCtrls.hpp>
#include <FMX.Objects.hpp>

typedef System::UnicodeString string;
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TIdHTTP *IdHTTP1;
	TTabControl *tabcontrolMain;
	TToolBar *ToolBar1;
	TTabItem *tabitemText;
	TTabItem *tabitemMemo;
	TMemo *memoMain;
	TVertScrollBox *vertScrol;
	TLabel *Label1;
	TButton *buttonInfo;
	TImage *Image1;
	void __fastcall tabcontrolMainChange(TObject *Sender);
	void __fastcall Label1Click(TObject *Sender);
	void __fastcall buttonInfoClick(TObject *Sender);

private:	// User declarations
	long double RangeOf;
	void Create_Label(long double vRange, int vX, int vY, string vText, int vWidth);
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
