//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TTheForm : public TForm
{
__published:	// IDE-managed Components
	TMemo *memo;
	TButton *button;
	TCheckBox *checkb;
	TRadioButton *radiob1;
	TEdit *edit;
	TRadioButton *radiob2;
	TTrackBar *TrackBar;
	TSwitch *Switch;
	void __fastcall buttonClick(TObject *Sender);
	void __fastcall checkbClick(TObject *Sender);
	void __fastcall radiob1Change(TObject *Sender);
	void __fastcall radiob2Change(TObject *Sender);
	void __fastcall TrackBarChange(TObject *Sender);
	void __fastcall SwitchSwitch(TObject *Sender);
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);

private:	// User declarations
public:		// User declarations
	__fastcall TTheForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTheForm *TheForm;
//---------------------------------------------------------------------------
#endif
