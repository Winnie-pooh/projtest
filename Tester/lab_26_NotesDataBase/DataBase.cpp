//---------------------------------------------------------------------------


#pragma hdrstop

#include "DataBase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TDataModule2 *DataModule2;


#include <System.IOUtils.hpp>
const UnicodeString cNameDB = "Notes.db";


//---------------------------------------------------------------------------
__fastcall TDataModule2::TDataModule2(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDataModule2::FDConnection1BeforeConnect(TObject *Sender)
{
	FDConnection1->Params->Values["Database"] =
#ifdef _Windows
		"..\\..\\" + cNameDB;
#else
		System::Ioutils::TPath::GetDocumentsPath()+PathDelim+cNameDB;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TDataModule2::FDConnection1AfterConnect(TObject *Sender)
{
	//FDConnection1->ExecSQL(
	//"CREATE TABLE IF NOT EXISTS [Notes]({)"
	//"[Caption] VARCHAR(50) NOT NULL,"
	//"[Priority] SMALLINT NOT NULL,"
	//"[Detail] VARCHAR(500))"
	//);
	FDTable1->Open();
}
//---------------------------------------------------------------------------
