//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "unitMain.h"
#include "DataBase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	TabControl1->ActiveTab = tabitemMain;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
	DataModule2->FDConnection1->Connected=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buttonAddClick(TObject *Sender)
{
	DataModule2->FDTable1->Append();
	TabControl1->GotoVisibleTab(tabitemSecret->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buttonSaveClick(TObject *Sender)
{
	DataModule2->FDTable1->Post();
	TabControl1->GotoVisibleTab(tabitemMain->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonCancelClick(TObject *Sender)
{
	DataModule2->FDTable1->Cancel();
	TabControl1->GotoVisibleTab(tabitemMain->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buttonDeleteClick(TObject *Sender)
{
	DataModule2->FDTable1->Delete();
	TabControl1->GotoVisibleTab(tabitemMain->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	DataModule2->FDTable1->Edit();
	TabControl1->GotoVisibleTab(tabitemSecret->Index);
}
//---------------------------------------------------------------------------

