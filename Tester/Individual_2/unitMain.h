//---------------------------------------------------------------------------
#ifndef unitMainH
#define unitMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.TabControl.hpp>
#include <System.Net.HttpClient.hpp>
#include <System.Net.HttpClientComponent.hpp>
#include <System.Net.URLClient.hpp>
#include <FMX.WebBrowser.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XmlTransform.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.XMLIntf.hpp>
#include <IdIOHandler.hpp>
#include <IdIOHandlerSocket.hpp>
#include <IdIOHandlerStack.hpp>
#include <IdSSL.hpp>
#include <IdSSLOpenSSL.hpp>
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdIOHandler.hpp>
#include <IdIOHandlerSocket.hpp>
#include <IdIOHandlerStack.hpp>
#include <IdSSL.hpp>
typedef System::UnicodeString string;
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TIdHTTP *IdHTTP1;
	TVertScrollBox *VertScrollBox1;
	TMemo *memoAll;
	TLayout *Layout1;
	TVertScrollBox *VertScrollBox2;
	TMemo *textTest;
	TTabControl *tabcontrolMain;
	TTabItem *tabitemHidden;
	TTabItem *tabitemMain;
	TImage *Image1;
	TButton *Button2;
	TImage *Image2;
	TLayout *Layout2;
	TVertScrollBox *vertscrollboxWriter;
	TTabItem *tabitemCreation;
	TVertScrollBox *vertscrollboxCreation;
	TLayout *Layout3;
	TImage *Image3;
	TImage *Image4;
	TButton *Button3;
	TLayout *Layout4;
	TLayout *Layout5;
	TTabItem *tabitemPoem;
	TLayout *Layout6;
	TImage *Image5;
	TButton *Button4;
	TLayout *Layout7;
	TVertScrollBox *vertscrollboxPoem;
	TText *textPoemCaption;
	TButton *Button5;
	TButton *Button6;
	TButton *buttonAboutWriter;
	TWebBrowser *WebBrowser1;
	TIdHTTP *IdHTTP2;
	TIdSSLIOHandlerSocketOpenSSL *IdSSLIOHandlerSocketOpenSSL1;
	TLabel *labelWriterName;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Label1Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Label2Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall buttonAboutWriterClick(TObject *Sender);
private:	// User declarations
			void Create_Label(int vX, int vY, string vText, int vWidth, TFmxObject* parent, string vName, int vSize, TNotifyEvent vOnClick);
			long int NameNumb;
            string ActiveWriter;
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
