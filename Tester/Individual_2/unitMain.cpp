//---------------------------------------------------------------------------
#include <fmx.h>
#pragma hdrstop
#include "unitMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
typedef System::UnicodeString string;
string *WritersLink;
string *PoemsLink;
#include <IdURI.hpp>
//---------------------------------------------------------------------------
inline int Low(const System::UnicodeString &)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return 1;
	#else
		return 0;
	#endif
}
inline int High(const System::UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
		return S.Length();
	#else
		return S.Length()-1;
	#endif
}
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	tabcontrolMain->ActiveTab = tabitemMain;
	NameNumb=1;
}

//---------------------------------------------------------------------------
void __fastcall TformMain::FormCreate(TObject *Sender)
{
	string url = "http://rupoem.ru/";
	memoAll->Text = IdHTTP1->Get(url);
	string valueStr="", ShouldFind="</option>", ParsString="";
	int ShouldFindPosition=1;
	string ImportantString = "", BreakPointString = "";
	for (int i = 2990; i<memoAll->Text.Length(); i++) {
		if (valueStr=="</option>")
		{
			if (ParsString==ShouldFind) //  value=\"
			{
				if (memoAll->Text[i]=='\"')
				{
					textTest->Lines->Add(ImportantString);
					ImportantString="";
					i++;
					continue;
				}
				if (memoAll->Text[i]=='<')
				{
					textTest->Lines->Add(ImportantString);
					ImportantString="";
					ParsString="";
					ShouldFindPosition=1;
					continue;
				}
				ImportantString+=memoAll->Text[i];
                continue;
			}
			// Find   value=\"
			if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
			{
				ParsString+=memoAll->Text[i];
				ShouldFindPosition++;
				continue;
			}
			else
			{
				ParsString="";
				ShouldFindPosition=1;
				if (memoAll->Text[i]=='<'||memoAll->Text[i]=='/'||memoAll->Text[i]=='s'||memoAll->Text[i]=='e'||memoAll->Text[i]=='l'||memoAll->Text[i]=='c'||memoAll->Text[i]=='t'||memoAll->Text[i]=='>')
				{
					BreakPointString+=memoAll->Text[i];
					if (BreakPointString=="</select>")
						break;
				}
				else
					BreakPointString="";
				continue;
			}
		}
		// Find </option>
		if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
		{
			valueStr+=memoAll->Text[i];
			ShouldFindPosition++;
			if (valueStr=="</option>")
			{
				ShouldFindPosition=1;
				ShouldFind="value=\"";
			}
		}
		else
		{
			valueStr="";
			ShouldFindPosition=1;
		}
	}
    int X=70, Y=40;
	int Width, Posish_old=NULL;
	string str="";
	for (int i=1; i<textTest->Lines->Count; i+=2)
	{
		str = textTest->Lines->Strings[i];
		Width=(str.Length())*12;
		Create_Label(X, Y, str, Width, vertscrollboxWriter, "Writer", 18, Label1Click);
		if(X >=vertscrollboxWriter->Width/2-40)
		{
			Y+=30;
			X=70;
			continue;
		}
		X = vertscrollboxWriter->Width/2+20;
	}
	WritersLink = new string[textTest->Lines->Count/2+2];
	int k=1;
	for(int i=0; i<textTest->Lines->Count; i+=2)
	{
		WritersLink[k]=textTest->Lines->Strings[i];
		k++;
	}
	NameNumb=1;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button2Click(TObject *Sender)
{
	MessageBoxA(0, "������ ���������� �������� ��������� ���������� ����� rupoem.ru\n��� ���������� ������ ���������� ������� �������� ���������� \n\n1. ������ �������� \n2. ������ ������������� \n3. �����������", "Information", 0);
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Label1Click(TObject *Sender)
{
	 TComponent* Text;
	 for (int i = 1; ; i++)
	 {
		Text = vertscrollboxCreation->FindComponent("Poem"+i);
		if(Text)
			((TText*)Text)->Free();
		else
		{
			Text = formMain->FindComponent("Poem"+IntToStr(i));
			if (Text)
				((TText*)Text)->Free();
			else
				break;
		}
	 }
	tabcontrolMain->ActiveTab=tabitemCreation;
	string url = "http://rupoem.ru"+WritersLink[((TText*)Sender)->Tag];
	memoAll->Lines->Clear();
	memoAll->Text = IdHTTP1->Get(url);
	textTest->Lines->Clear();
	string valueStr="", ShouldFind="class=\"catlink\"", ParsString="";
	int ShouldFindPosition=1;
	string ImportantString = "", BreakPointString = "";
	bool flag=true, flagers=false;
	for (int i = 3000; i<memoAll->Text.Length(); i++) {
		if (valueStr=="class=\"catlink\"")
		{
			if (ParsString==ShouldFind) //  href=\'
			{
				if (memoAll->Text[i]=='>')
				{
					ImportantString="";
					flagers=true;
					continue;
				}
				if (memoAll->Text[i]=='\''&&flag)
				{
					textTest->Lines->Add(ImportantString);
					ImportantString="";
					flag=false;
					continue;
				}
				if (memoAll->Text[i]=='<')
				{
					textTest->Lines->Add(ImportantString);
					ImportantString="";
					ParsString="";
					ShouldFindPosition=1;
					flag=true;
					flagers=true;
					continue;
				}
				if ((memoAll->Text[i]==' '||memoAll->Text[i]=='\r'||memoAll->Text[i]=='\n')&&flagers)
				{
					if (ImportantString!="")
					{
						flagers=false;
						i--;
					}
					continue;
				}
				ImportantString+=memoAll->Text[i];
				continue;
			}
			// Find   href=\'
			if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
			{
				ParsString+=memoAll->Text[i];
				ShouldFindPosition++;
				continue;
			}
			else
			{
				ParsString="";
				ShouldFindPosition=1;
				if (memoAll->Text[i]=='<'||memoAll->Text[i]=='/'||memoAll->Text[i]=='t'||memoAll->Text[i]=='a'||memoAll->Text[i]=='b'||memoAll->Text[i]=='l'||memoAll->Text[i]=='e'||memoAll->Text[i]=='>')
				{
					BreakPointString+=memoAll->Text[i];
					if (BreakPointString=="</table>")
						break;
				}
				else
					BreakPointString="";
				continue;
			}
		}
		// Find class=\"catlink\"
		if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
		{
			valueStr+=memoAll->Text[i];
			ShouldFindPosition++;
			if (valueStr=="class=\"catlink\"")
			{
				ShouldFindPosition=1;
				ShouldFind="href=\'";
			}
		}
		else
		{
			valueStr="";
			ShouldFindPosition=1;
		}
	}
	int X=14, Y=50;
	int Width, Posish_old=NULL;
	string str="";
	for (int i=1; i<textTest->Lines->Count; i+=2)
	{
		str = textTest->Lines->Strings[i];
		Width=(str.Length())*13;
		Create_Label(X, Y, str, Width, vertscrollboxCreation, "Poem", 16, Label2Click);
		if(X >=vertscrollboxWriter->Width/2-40||Width>=vertscrollboxWriter->Width/2+100)
		{
			Y+=35;
			X=14;
			continue;
		}
		X = vertscrollboxWriter->Width/2-20;
	}
	PoemsLink = new string[textTest->Lines->Count/2+2];
	int k=1;
	for(int i=0; i<textTest->Lines->Count; i+=2)
	{
		PoemsLink[k]=textTest->Lines->Strings[i];
		k++;
	}
	NameNumb=1;
	ActiveWriter=((TText*)Sender)->Text;
	labelWriterName->Text=((TText*)Sender)->Text;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Label2Click(TObject *Sender)
{
     TComponent* Text;
	 for (int i = 1; ; i++)
	 {
		Text = vertscrollboxCreation->FindComponent("Problem"+i);
		if(Text)
			((TText*)Text)->Free();
		else
		{
			Text = formMain->FindComponent("Problem"+IntToStr(i));
			if (Text)
				((TText*)Text)->Free();
			else
				break;
		}
	 }
	tabcontrolMain->ActiveTab=tabitemPoem;
	string url = "http://rupoem.ru"+PoemsLink[((TText*)Sender)->Tag];
	memoAll->Lines->Clear();
	memoAll->Text = IdHTTP1->Get(url);
	textTest->Lines->Clear();
	string valueStr="", ShouldFind="<pre>", ParsString="";
	int ShouldFindPosition=1;
	string ImportantString = "";
	bool flag=false;
	for (int i = 3000; i<memoAll->Text.Length(); i++) {
		if (valueStr=="<pre>")
		{
			if (memoAll->Text[i]=='>')
			{
				if (ParsString==ShouldFind)
				{
					textTest->Lines->Add(ImportantString);
					break;
				}
				else
				{
					ParsString="";
					flag=false;
					ImportantString="";
					continue;
				}
			}
			if (memoAll->Text[i]=='<')
			{
				ParsString+=memoAll->Text[i];
				flag=true;
				continue;
			}
			if (!flag)
			{
				if (memoAll->Text[i]=='\r')
				{
					textTest->Lines->Add(ImportantString);
					ImportantString="";
					i++;
					continue;
				}
				ImportantString+=memoAll->Text[i];
				if (ImportantString==" ")
					ImportantString="";
			}
			else
				ParsString+=memoAll->Text[i];
			continue;
		}
		// Find <pre>
		if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
		{
			valueStr+=memoAll->Text[i];
			ShouldFindPosition++;
			if (valueStr=="<pre>")
			{
				ShouldFindPosition=1;
				ShouldFind="</pre";
			}
		}
		else
		{
			valueStr="";
			ShouldFindPosition=1;
		}
	}
	TText *label;
	int X, Y=-10;
	NameNumb=1 ;
	string str="";
	for(int i=0; i<textTest->Lines->Count;i++)
	{
		str = textTest->Lines->Strings[i];
        label = new TText(this);
		label->Text = str;
		X=vertscrollboxPoem->Width/2-(label->Text.Length()*14)/3;
		label->Position->X=X;
		label->Position->Y=Y;
		label->TextSettings->Font->Size=20;
		label->HorzTextAlign=(TTextAlign)"Leading";
		label->Height=20*2;
		label->Width=(label->Text.Length()*14);
		label->Name = "Problem"+(string)NameNumb;
		label->Parent = vertscrollboxPoem;
		NameNumb++;
		Y+=30;
	}
	textPoemCaption->Text=((TText*)Sender)->Text;
	NameNumb=1;
}
//---------------------------------------------------------------------------
void TformMain::Create_Label(int vX, int vY, string vText, int vWidth, TFmxObject* parent, string vName, int vSize, TNotifyEvent vOnClick)
{
	TText *label;
	label = new TText(this);
	label->Text = vText;
	label->Position->X=vX;
	label->Position->Y=vY;
	label->TextSettings->Font->Size=vSize;
	label->HorzTextAlign=(TTextAlign)"Leading";
	label->Height=vSize*2-vSize/4;
	label->Width=vWidth;
	label->HitTest=true;
	label->Tag = NameNumb;
	label->OnClick = vOnClick;
	label->Name = vName+NameNumb;
	label->Parent = parent;
	NameNumb++;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button4Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab=tabitemCreation;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button3Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab=tabitemMain;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button5Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab=tabitemPoem;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button6Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab=tabitemCreation;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonAboutWriterClick(TObject *Sender)
{
	if (ActiveWriter.IsEmpty())
		return;
	string url = "https://ru.wikipedia.org/w/api.php?action=opensearch&search="+ActiveWriter+"&prop=info&format=xml&inprop=url";
	WebBrowser1->URL=url;
	ActiveWriter = AnsiToUtf8(ActiveWriter);
	url ="https://ru.wikipedia.org/w/api.php?action=opensearch&search=";
	url+= TIdURI::ParamsEncode(ActiveWriter);
	url+= "&prop=info&format=xml&inprop=url";
	IdHTTP2->IOHandler = IdSSLIOHandlerSocketOpenSSL1;
	memoAll->Text = IdHTTP2->Get(url);
	string valueStr="", ShouldFind="<Description";
	int ShouldFindPosition=1;
	string ImportantString = "";
	bool flag=false, Doubleflag=false;
	for (int i = 20; i<memoAll->Text.Length(); i++) {
		if (valueStr=="<Description")
		{
			if (memoAll->Text[i]=='['||memoAll->Text[i]==']')
			{
				Doubleflag=!Doubleflag;
				continue;
			}
			if (Doubleflag)
				continue;
			if (memoAll->Text[i]=='>')
			{
				flag=true;
				continue;
			}
			if (memoAll->Text[i]=='<')
				break;
			if (flag)
				ImportantString+=memoAll->Text[i];
			continue;
		}
		// Find <Description
		if (memoAll->Text[i]==ShouldFind[ShouldFindPosition])
		{
			valueStr+=memoAll->Text[i];
			ShouldFindPosition++;
		}
		else
		{
			valueStr="";
			ShouldFindPosition=1;
		}
	}
	//char* mas = new char[ImportantString.Length()];
	//for (int i = 1; i < ImportantString.Length(); i++)
		//mas[i]=ImportantString[i];
	//MessageBoxA(0, mas, "� ��������", 0);
	if (ImportantString.IsEmpty()||ImportantString=="")
		ImportantString="Unknow";
	else
		ImportantString+="\n\n\t\t\t�Wikipedia.org";
	ShowMessage(ImportantString);
}
//---------------------------------------------------------------------------

