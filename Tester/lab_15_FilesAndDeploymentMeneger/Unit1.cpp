//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;

#include <System.IOUtils.hpp>

typedef System::UnicodeString string;

//const string cPath =
	//#ifdef _Windows
		//"Materials\\";
   //	#else
	   //	System::Ioutils::TPath::GetDocumentsPath();
   //	#endif

const string cList[] =  {"coupe", "hardtop", "hatchback", "liftback", "limousine",
							"minibus", "minivan", "sedan", "universal"
						};

//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	cPath =
	#ifdef _Windows
		"Materials\\";
	#else
		System::Ioutils::TPath::GetDocumentsPath();
	#endif



    tabcontrolMain->ActiveTab =  tabitemMain;
}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonBackClick(TObject *Sender)
{
	tabcontrolMain->ActiveTab =  tabitemMain;
}
//---------------------------------------------------------------------------

void TformMain::LoadItem(int vIndex)
{
	string xName = System::Ioutils::TPath::Combine(cPath, cList[vIndex]);

	imageMain->Bitmap->LoadFromFile(xName+".jpg");

	TStringList *x = new TStringList;
	try{
		x->LoadFromFile(xName+".txt");
		textMain->Text = x->Text;
	}
	__finally{
		x->DisposeOf();
    }
}

void __fastcall TformMain::ListBox1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	scrollboxMain->ViewportPosition = TPointF(0,0);
	LoadItem(Item->Index);
	textMain->RecalcSize();
	tabcontrolMain->ActiveTab = tabitemSources;
}
//---------------------------------------------------------------------------
