//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	Randomize();
	buttonRandomClick(buttonRandom);
}
//---------------------------------------------------------------------------

void TformMain::CreateColor(TAlphaColor value)
{
	formMain->Fill->Color=value;
	Label1->Text=AlphaColorToString(value);
	Label2->Text=Format("%d,%d,%d", ARRAYOFCONST((TAlphaColorRec(value).R, TAlphaColorRec(value).G, TAlphaColorRec(value).B)));
}

void __fastcall TformMain::buttonRandomClick(TObject *Sender)
{

	CreateColor(TAlphaColorF::Create(Random(256), Random(256), Random(256)).ToAlphaColor());
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonEpilepticClick(TObject *Sender)
{
	timerMain->Enabled=!timerMain->Enabled;
    buttonRandom->Enabled=!buttonRandom->Enabled;

}
//---------------------------------------------------------------------------

void __fastcall TformMain::timerMainTimer(TObject *Sender)
{
	buttonRandomClick(buttonRandom);
}
//---------------------------------------------------------------------------

