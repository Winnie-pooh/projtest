//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>

#include <System.Math.hpp>
#include <System.UIConsts.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buttonRandom;
	TButton *buttonEpileptic;
	TTimer *timerMain;
	TLabel *Label1;
	TLabel *Label2;
	void __fastcall buttonRandomClick(TObject *Sender);
	void __fastcall buttonEpilepticClick(TObject *Sender);
	void __fastcall timerMainTimer(TObject *Sender);
private:	// User declarations

	void CreateColor(TAlphaColor value);

public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
