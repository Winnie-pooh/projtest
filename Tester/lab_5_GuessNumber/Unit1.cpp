//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
 	Randomize();
	MinNumber = 1000;
	MaxNumber = 9999;
	QuantityNumber = 4;

	int i;
	for(i=0; i<12; i++)
	{
		Success[i]=0;
	}
}
//---------------------------------------------------------------------------


void __fastcall TformMain::timerMainTimer(TObject *Sender)
{
	progressbarMain->Value++;
	if(progressbarMain->Value>=progressbarMain->Max)
	{
		timerMain->Enabled=false;
		progressbarMain->Value=0;
		labelNumber->Text="";
		editAnswer->Visible=true;
		buttonAnswer->Visible=true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonMinusClick(TObject *Sender)
{
	if (QuantityNumber<=2) {
		return;
	}
	QuantityNumber--;
	MaxNumber-=MinNumber*9;
	MinNumber/=10;
	editQuantityNumber->Text = IntToStr(QuantityNumber);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonPlusClick(TObject *Sender)
{
	if (QuantityNumber>=10) {
		return;
	}
	QuantityNumber++;

	MinNumber*=10;
	MaxNumber+=MinNumber*9;

	editQuantityNumber->Text = IntToStr(QuantityNumber);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonStartClick(TObject *Sender)
{
	rectangleManipulation->Visible=false;
	rectangleTimeManipulation->Visible=false;
	buttonStart->Enabled=false;

	labelSuccess->Text="";

	RandomNumber = RandomRange(MinNumber, MaxNumber);
	labelNumber->Text = RandomNumber;

	timerMain->Enabled=true;

	buttonReset->Enabled=false;
	expanderResults->IsExpanded=false;
}
//---------------------------------------------------------------------------


void __fastcall TformMain::buttonTimeMinusClick(TObject *Sender)
{
	if (progressbarMain->Max<=10) {
		return;
	}

	progressbarMain->Max-=10;
	//Kostil'
	int chislo =  progressbarMain->Max/10;
	editSeconds->Text = IntToStr(chislo);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonTimePlusClick(TObject *Sender)
{
		if (progressbarMain->Max>=10*10) {
		return;
	}

	progressbarMain->Max+=10;
	//Kostil'
	int chislo =  progressbarMain->Max/10;
	editSeconds->Text = IntToStr(chislo);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonAnswerClick(TObject *Sender)
{
	int x = StrToIntDef(editAnswer->Text, 0);

	if (x==RandomNumber) {
		Success[QuantityNumber]++;

		labelSuccess->Text = "�����";
	}
	else
	{
		Success[QuantityNumber]--;

		labelSuccess->Text = "������ �_�";
	}

	buttonAnswer->Visible=false;
	editAnswer->Visible=false;
    editAnswer->Text="";
	buttonStart->Enabled=true;

	rectangleManipulation->Visible=true;
	rectangleTimeManipulation->Visible=true;

	buttonReset->Enabled=true;
	expanderResults->IsExpanded=false;


	TComponent* LabelResult;
	LabelResult = formMain->FindComponent("labelResult"+IntToStr(QuantityNumber));
	if(LabelResult)
	{
		((TLabel*)LabelResult)->Text=IntToStr(QuantityNumber)+" -> "+IntToStr(Success[QuantityNumber]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonResetClick(TObject *Sender)
{
	TComponent* LabelResult;

	for(int i=2; i<12; i++)
	{
		Success[i]=0;
		LabelResult = formMain->FindComponent("labelResult"+IntToStr(i));
        if(LabelResult)
		{
			((TLabel*)LabelResult)->Text=IntToStr(i)+" -> "+IntToStr(Success[i]);
		}
	}

}
//---------------------------------------------------------------------------

void __fastcall TformMain::editAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key==vkReturn) {
		TformMain::buttonAnswerClick(Sender);
	}
}
//---------------------------------------------------------------------------

