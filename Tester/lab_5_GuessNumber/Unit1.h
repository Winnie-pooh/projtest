//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
#include <FMX.TreeView.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.ListBox.hpp>

#include <System.Math.hpp>
#include <System.UIConsts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TLayout *layoutMain;
	TRectangle *rectangleNumber;
	TLabel *labelNumber;
	TProgressBar *progressbarMain;
	TEdit *editAnswer;
	TButton *buttonAnswer;
	TButton *buttonMinus;
	TButton *buttonPlus;
	TLabel *Label1;
	TRectangle *rectangleManipulation;
	TTimer *timerMain;
	TButton *buttonStart;
	TRectangle *rectangleTimeManipulation;
	TButton *buttonTimeMinus;
	TButton *buttonTimePlus;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *editSeconds;
	TLabel *Label4;
	TLabel *Label5;
	TEdit *editQuantityNumber;
	TLabel *labelSuccess;
	TToolBar *ToolBar1;
	TButton *buttonReset;
	TExpander *expanderResults;
	TLabel *labelResult2;
	TLabel *labelResult5;
	TLabel *labelResult3;
	TLabel *labelResult4;
	TLabel *labelResult6;
	TLabel *labelResult7;
	TLabel *labelResult8;
	TLabel *labelResult9;
	TLabel *labelResult10;
	void __fastcall timerMainTimer(TObject *Sender);
	void __fastcall buttonMinusClick(TObject *Sender);
	void __fastcall buttonPlusClick(TObject *Sender);
	void __fastcall buttonStartClick(TObject *Sender);
	void __fastcall buttonTimeMinusClick(TObject *Sender);
	void __fastcall buttonTimePlusClick(TObject *Sender);
	void __fastcall buttonAnswerClick(TObject *Sender);
	void __fastcall buttonResetClick(TObject *Sender);
	void __fastcall editAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);

private:	// User declarations

	int MinNumber;
	int MaxNumber;
	int QuantityNumber;
	int RandomNumber;

	int Success[11];

public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
