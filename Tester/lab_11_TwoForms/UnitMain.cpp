//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "UnitMain.h"
#include "UnitAbout.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	tabcontrolMain->ActiveTab = tabitemMain;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonGoOutClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonAboutClick(TObject *Sender)
{
	formMain->Hide();
	formAbout->ShowModal();

}
//---------------------------------------------------------------------------
void __fastcall TformMain::Button1Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemLevel;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::Button2Click(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tebitemRules;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonBackClick(TObject *Sender)
{
	tabcontrolMain->ActiveTab = tabitemMain;
}
//---------------------------------------------------------------------------

