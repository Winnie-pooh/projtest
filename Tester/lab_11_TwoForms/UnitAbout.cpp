//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "UnitAbout.h"
#include "UnitMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformAbout *formAbout;
//---------------------------------------------------------------------------
__fastcall TformAbout::TformAbout(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TformAbout::buttonBackClick(TObject *Sender)
{
	Close();
    formMain->Show();
}
//---------------------------------------------------------------------------
