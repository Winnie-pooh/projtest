//---------------------------------------------------------------------------

#ifndef UnitMainH
#define UnitMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tabcontrolMain;
	TTabItem *tabitemMain;
	TTabItem *tabitemLevel;
	TTabItem *tebitemRules;
	TLayout *Layout1;
	TButton *Button1;
	TButton *Button2;
	TButton *buttonAbout;
	TButton *buttonGoOut;
	TGridPanelLayout *GridPanelLayout;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TToolBar *ToolBar1;
	TButton *buttonBack;
	TLabel *Label1;
	TToolBar *ToolBar2;
	TButton *Button3;
	TLabel *Label2;
	void __fastcall buttonGoOutClick(TObject *Sender);
	void __fastcall buttonAboutClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall buttonBackClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
