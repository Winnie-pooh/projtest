//---------------------------------------------------------------------------

#ifndef UnitAboutH
#define UnitAboutH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TformAbout : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buttonBack;
	void __fastcall buttonBackClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TformAbout(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformAbout *formAbout;
//---------------------------------------------------------------------------
#endif
