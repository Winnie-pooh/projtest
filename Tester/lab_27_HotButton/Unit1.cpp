//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonMouseEnter(TObject *Sender)
{
	TButton *x = ((TButton *)Sender);
	x->Margins->Rect = TRect (0,0,0,0);
	x->TextSettings->Font->Size +=14;
	x->TextSettings->Font->Style = x->TextSettings->Font->Style << TFontStyle::fsBold;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonMouseLeave(TObject *Sender)
{
	TButton *x = ((TButton *)Sender);
	x->Margins->Rect = TRect (20,20,20,20);
	x->TextSettings->Font->Size -=14;
	x->TextSettings->Font->Style = x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RectangleMouseEnter(TObject *Sender)
{
	TRectangle *x = ((TRectangle *)Sender);
	//TAnimator::AnimateInt(x, "Position.X", 0, 1, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RectangleMouseLeave(TObject *Sender)
{
	TRectangle *x = ((TRectangle *)Sender);
	x->Position->X = -400;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ImageMouseEnter(TObject *Sender)
{
	TImage *x = ((TImage *)Sender);
	x->Margins->Rect = TRect (20,20,20,20);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ImageMouseLeave(TObject *Sender)
{
	TImage *x = ((TImage *)Sender);
	x->Margins->Rect = TRect (0,0,0,0);
}
//---------------------------------------------------------------------------

