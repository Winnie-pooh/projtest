//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TRectangle *Rectangle1;
	TLayout *Layout1;
	TLayout *Layout2;
	TImage *Image1;
	TImage *Image2;
	TFloatAnimation *FloatAnimation1;
	void __fastcall ButtonMouseEnter(TObject *Sender);
	void __fastcall ButtonMouseLeave(TObject *Sender);
	void __fastcall RectangleMouseEnter(TObject *Sender);
	void __fastcall RectangleMouseLeave(TObject *Sender);
	void __fastcall ImageMouseEnter(TObject *Sender);
	void __fastcall ImageMouseLeave(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
