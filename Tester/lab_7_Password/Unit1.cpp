//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "UnitPasswordGenerate.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
	Randomize();

}
//---------------------------------------------------------------------------
void __fastcall TformMain::buttonClearClick(TObject *Sender)
{
    editContains->Text="";
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonGenerateClick(TObject *Sender)
{
	editPassword->Text= PasswordGenerate(numberboxLength->Value, checkboxLoweraCase->IsChecked,
	checkboxUpperCase->IsChecked, checkboxNumbers->IsChecked, checkboxSymbols->IsChecked,
	checkboxStranges->IsChecked, editContains->Text);

}
//---------------------------------------------------------------------------

