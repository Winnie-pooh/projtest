//---------------------------------------------------------------------------

#ifndef UnitPasswordGenerateH
#define UnitPasswordGenerateH
//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
//--------------------------------------------------------------------------
//using str = System::UnicodeString;
//namespacce str = System;



System::UnicodeString PasswordGenerate(int valueLength,bool valueLoweraCase,
	bool valueUpperCase, bool valueNumbers, bool valueSymbols, bool valueStranges, System::UnicodeString valueContains);


#endif
