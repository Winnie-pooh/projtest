//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>

#include <Math.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TLayout *layoutMain;
	TEdit *editPassword;
	TButton *buttonGenerate;
	TCheckBox *checkboxLoweraCase;
	TCheckBox *checkboxUpperCase;
	TCheckBox *checkboxNumbers;
	TCheckBox *checkboxSymbols;
	TNumberBox *numberboxLength;
	TLabel *labelLength;
	TEdit *editContains;
	TLabel *labelContains;
	TButton *buttonClear;
	TCheckBox *checkboxStranges;
	void __fastcall buttonClearClick(TObject *Sender);
	void __fastcall buttonGenerateClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
