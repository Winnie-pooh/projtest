//---------------------------------------------------------------------------

#pragma hdrstop

#include "UnitPasswordGenerate.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

int valueLength=6;
bool valueLoweraCase = false;
bool valueUpperCase = false;
bool valueNumbers = false;
bool valueSymbols = false;
bool valueStranges = false;
System::UnicodeString valueContains = "";


System::UnicodeString PasswordGenerate(int valueLength, bool valueLoweraCase,
	bool valueUpperCase, bool valueNumbers, bool valueSymbols, bool valueStranges, System::UnicodeString valueContains)
{
	System::UnicodeString password="", symbols="";
	const char *cLetters = "abcdefghijklmnopqrstuvwxyz";
	const char *cNumbers = "0123456789";
	const char *cSymbols = "[]{}<>,.;:-+#";

	if (valueLoweraCase) symbols+=cLetters;
	if (valueUpperCase) symbols+=UpperCase(cLetters);
	if (valueNumbers) symbols+=cNumbers;
	if (valueSymbols) symbols+=cSymbols;

	if (symbols.IsEmpty()&&!valueStranges) return valueContains;


	//ShowMessage(sizeof(System::UnicodeString));
	char sim;
	bool flagContains = true;
	int randContains;
			   //System::UnicodeString str =sim;
			   //ShowMessage(str);

	while(password.Length()<valueLength)
	{
		randContains = Random(99);

		if ((((valueLength-password.Length())<=valueContains.Length())|| (randContains>=50)) &&(flagContains))
		{
			password+=valueContains;
			flagContains = false;
			continue;
		}

		if (!valueStranges)
		{
			password+= symbols.SubString(Random(symbols.Length()+1), 1);
			continue;
		}

		if (symbols.IsEmpty())
		{
			sim =1+ Random(256*sizeof(System::UnicodeString)-2);
			password+=sim;
			continue;
		}

		if (randContains<50) {
		   sim =1+ Random(256*sizeof(System::UnicodeString)-2);
		   password+=sim;
		}
		else
			password+= symbols.SubString(Random(symbols.Length()+1), 1);

	}

	return password;
}


