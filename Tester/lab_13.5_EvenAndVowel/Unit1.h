//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tabcontrolMain;
	TTabItem *tabitemMain;
	TTabItem *tabitemGame;
	TTabItem *tabitemRezult;
	TTabItem *tabitemHelp;
	TLayout *Layout1;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buttonStart;
	TButton *buttonHelp;
	TButton *buttonGoOut;
	TToolBar *ToolBar1;
	TButton *buttonGoToMenu;
	TLabel *labelPoints;
	TLabel *labelTime;
	TLabel *Label4;
	TTimer *timerMain;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label5;
	TRectangle *Rectangle1;
	TLabel *labelEven;
	TLabel *Label7;
	TRectangle *Rectangle2;
	TLabel *labelVowel;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buttonYes;
	TButton *buttonNo;
	TLayout *Layout3;
	TLabel *Label2;
	TLabel *labelGood;
	TLabel *labelBad;
	TButton *buttonRepit;
	TButton *buttonToMenu;
	TToolBar *ToolBar2;
	TButton *buttonToMenu1;
	TStyleBook *StyleBook1;
	void __fastcall buttonGoOutClick(TObject *Sender);
	void __fastcall buttonHelpClick(TObject *Sender);
	void __fastcall GoToMenuClick(TObject *Sender);
	void __fastcall StartClick(TObject *Sender);
	void __fastcall MainTimer(TObject *Sender);
	void __fastcall YesClick(TObject *Sender);
	void __fastcall NoClick(TObject *Sender);
private:	// User declarations
			double TimeValue;
			bool Answer;
			int Correct, UnCorrect;

			void CreateTheQuestion();

public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
