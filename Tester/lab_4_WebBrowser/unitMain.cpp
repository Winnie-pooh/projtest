//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "unitMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	browser->URL="ya.ru";
	editAddress->Text=browser->URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::editAddressKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if(Key==vkReturn)
	{
        browser->URL=editAddress->Text;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonSearchClick(TObject *Sender)
{
    browser->URL=editAddress->Text;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonNextClick(TObject *Sender)
{
    browser->GoForward();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonBackClick(TObject *Sender)
{
    browser->GoBack();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::browserDidStartLoad(TObject *ASender)
{
	editAddress->Text=browser->URL;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::browserDidFinishLoad(TObject *ASender)
{
    editAddress->Text=browser->URL;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonHomeClick(TObject *Sender)
{
    browser->URL="yandex.ru";
}
//---------------------------------------------------------------------------

