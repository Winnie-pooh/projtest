//---------------------------------------------------------------------------

#ifndef unitMainH
#define unitMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
#include <FMX.Menus.hpp>
#include <FMX.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *toolbarBottom;
	TWebBrowser *browser;
	TToolBar *toolbarTop;
	TEdit *editAddress;
	TButton *buttonSearch;
	TButton *buttonNext;
	TButton *buttonBack;
	TButton *buttonHome;
	void __fastcall editAddressKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall buttonSearchClick(TObject *Sender);
	void __fastcall buttonNextClick(TObject *Sender);
	void __fastcall buttonBackClick(TObject *Sender);
	void __fastcall browserDidStartLoad(TObject *ASender);
	void __fastcall browserDidFinishLoad(TObject *ASender);
	void __fastcall buttonHomeClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
