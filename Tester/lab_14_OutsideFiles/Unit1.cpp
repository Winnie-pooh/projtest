//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;

typedef System::UnicodeString string;


//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void LoadResourceToImage(string vResName, TBitmap* vResult)
{
	TResourceStream* x;
	x = new TResourceStream((int)HInstance, vResName, RT_RCDATA);
	try{
		vResult->LoadFromStream(x);
	}
	__finally{
		delete x;
    }
}

string LoadResourceToText (string vResName)
{
	TResourceStream* x;
	TStringStream* xSS;

	x = new TResourceStream ((int)HInstance, vResName, RT_RCDATA);
	try{
		xSS = new TStringStream("", TEncoding::UTF8, true);
		try{
			xSS->LoadFromStream(x);
			return xSS->DataString;
		}
		__finally{
			delete xSS;
		}
	}
	__finally{
		delete x;
	}
}




void __fastcall TformMain::ClearClick(TObject *Sender)
{
	imageMain->Bitmap->SetSize(0,0);
	memoMain->Lines->Clear();
}
//---------------------------------------------------------------------------


void __fastcall TformMain::Button1Click(TObject *Sender)
{
	LoadResourceToImage("img_"+IntToStr(((TControl *)Sender)->Tag), imageMain->Bitmap);
	memoMain->Lines->Text = LoadResourceToText("text_"+IntToStr(((TControl *)Sender)->Tag));
}
//---------------------------------------------------------------------------

