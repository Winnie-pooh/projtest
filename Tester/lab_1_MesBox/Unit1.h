//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TTheForm : public TForm
{
__published:	// IDE-managed Components
	TEdit *editText;
	TButton *butMain;
	void __fastcall butMainClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TTheForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTheForm *TheForm;
//---------------------------------------------------------------------------
#endif
