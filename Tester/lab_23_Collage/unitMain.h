//---------------------------------------------------------------------------

#ifndef unitMainH
#define unitMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buttonNew;
	TButton *buttonClear;
	TButton *Button3;
	TToolBar *toolbarMain;
	TButton *buttonBackImage;
	TButton *buttonTrash;
	TButton *buttonToBackward;
	TButton *buttonToFront;
	TButton *buttonNextImage;
	TTrackBar *trackbarRotation;
	TLayout *ly;
	void __fastcall Selection1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buttonNewClick(TObject *Sender);
	void __fastcall buttonClearClick(TObject *Sender);
	void __fastcall lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buttonBackImageClick(TObject *Sender);
	void __fastcall buttonNextImageClick(TObject *Sender);
	void __fastcall buttonToFrontClick(TObject *Sender);
	void __fastcall buttonToBackwardClick(TObject *Sender);
	void __fastcall trackbarRotationChange(TObject *Sender);
	void __fastcall buttonTrashClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
			TSelection *FSel;
			void ReSetSelection (TObject *Sender);
public:		// User declarations
	__fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
