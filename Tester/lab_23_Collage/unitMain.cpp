//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "unitMain.h"
#include "unitDataModule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TformMain *formMain;
//---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TformMain::Selection1MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, float X, float Y)
{
	ReSetSelection(Sender);
}
//---------------------------------------------------------------------------

void TformMain::ReSetSelection (TObject *Sender)
{
	FSel = dynamic_cast<TSelection*>(Sender);
	TSelection *x;
	for(int i=0; i<ly->ComponentCount; i++)
	{
		x=dynamic_cast<TSelection*>(ly->Components[i]);
		if(x)
			x->HideSelection = (x!=FSel);
	}

	toolbarMain->Visible = (FSel!=NULL);
	if (toolbarMain->Visible)
		trackbarRotation->Value = FSel->RotationAngle;
}
void __fastcall TformMain::buttonNewClick(TObject *Sender)
{
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->Width = 50+Random(100);
	x->Height = 50+Random(100);
	x->Position->X = Random(ly->Width - x->Width);
	x->Position->Y = Random(ly->Height - x->Height);
	x->RotationAngle= RandomRange(-90, 90);
	x->OnMouseDown = Selection1MouseDown;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = formDM->il;
	xGlyph->ImageIndex = Random(formDM->il->Count);

	ReSetSelection(x);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonClearClick(TObject *Sender)
{
	ReSetSelection(ly);
	for (int i = ly->ComponentCount-1; i>=0; i--)
	{
		if(dynamic_cast<TSelection*>(ly->Components[i]))
		{
			ly->RemoveObject(i);
        }

	}

}
//---------------------------------------------------------------------------

void __fastcall TformMain::lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
    ReSetSelection(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonBackImageClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex<=0)? formDM->il->Count-1:(int)x->ImageIndex-1;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonNextImageClick(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int)x->ImageIndex>=formDM->il->Count-1)? 0: (int)x->ImageIndex+1;;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonToFrontClick(TObject *Sender)
{
	FSel->BringToFront();
	FSel->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonToBackwardClick(TObject *Sender)
{
    FSel->SendToBack();
	FSel->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TformMain::trackbarRotationChange(TObject *Sender)
{
    FSel->RotationAngle = trackbarRotation->Value;
}
//---------------------------------------------------------------------------

void __fastcall TformMain::buttonTrashClick(TObject *Sender)
{
	FSel->DisposeOf();
	FSel = NULL;
    ReSetSelection(FSel);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::FormCreate(TObject *Sender)
{
    	ReSetSelection(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TformMain::Button3Click(TObject *Sender)
{
    ShowMessage("Hello");
}
//---------------------------------------------------------------------------

