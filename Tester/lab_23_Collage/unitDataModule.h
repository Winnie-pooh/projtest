//---------------------------------------------------------------------------

#ifndef unitDataModuleH
#define unitDataModuleH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class TformDM : public TDataModule
{
__published:	// IDE-managed Components
	TImageList *il;
private:	// User declarations
public:		// User declarations
	__fastcall TformDM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformDM *formDM;
//---------------------------------------------------------------------------
#endif
